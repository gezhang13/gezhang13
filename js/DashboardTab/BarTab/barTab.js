$(document).ready(function() { 

	$(".barTab #menu1 li a").click(function(){
		selectedBar = null;
		highlightedCity = null;
		secondSelectedCountry = null;
		highlightCountry(null, features);
		updateDetails();
		var strArray = $(this).text().split(" ");
		var parsedString = stringParser(strArray);
		$(".barTab #dropDownText").text(parsedString);
		$(".barTab #dd-text").text($(this).text());
		var fieldVal = textToID($(this).text()); 
		updateBarChartData(fieldVal);
   	});

	var highlightedCity = null;
	colorScale = d3.scale.linear().range(["#CCBCA2", "#63584A"]); // or use hex values;
	var xOffset = 3, widthOffset = 120, heightOffset = 6;
	var mapChart = d3.select("#chart_Sec svg");
	var currentvalue;
	// var tipStr = "Bhanu";
	// var infoTip = d3.tip()
 //  		.attr('class', 'd3-tip')
 //  		.offset([200, 200])
	// 	.html(function(d) {
	// 	    return "<strong>Frequency:</strong> <span style='color:red'>" + tipStr + "</span>";
	// 	});

	var numberFormat = (function() {
		var short_fmt = d3.format(",.0f");
		var nfmt = d3.format(",.1f");
		var fmt = function(v) {  // remove trailing .0
			var formatted = nfmt(v);
			var m = formatted.match(/^(.*)\.0$/);
			if (m !== null) formatted = m[1];
				return formatted;
		};
		return function(v) {
		if (v == null  ||  isNaN(v)) return msg("amount.not-available");
		if (isPlural(v, 1e9)) return msg("amount.billions",  fmt(v / 1e9));
		if (v >= 1e9) return msg("amount.billions.singular",  fmt(v / 1e9));
		if (isPlural(v, 1e6)) return msg("amount.millions",  fmt(v / 1e6));
		if (v >= 1e6) return msg("amount.millions.singular",  fmt(v / 1e6));
			return short_fmt(v);
		};
	})();

	// width and height of all svg containers
	var width = $("#mapThirdRow").width() - widthOffset; // hack for getting the width from the active tab
	var height = $("#barSecondRow").height() - heightOffset;

	// container for the bar chart
	var svgContainerBarChart = d3.select("#barChart")
		.append("svg")
	    .attr("width", width+40)
	    .attr("height", height)
		.attr("transform", "translate(0,0)");

	var div = d3.select("#barCol").append("div")
	    .attr("class", "pinwheelTooltip")
	    .style("opacity", 1e-6);

	var isPlural = function(v, exp) {
		var v = Math.abs(Math.round(v/exp));
		return v >= 2;
	}

	function textToID(strVal) {
		var res = "";
		if (strVal == "Percent of Fulton Population") {
			res = "totpop10";
		}
		else if(strVal == "Percent of One Race Total") {
			res = "oner_10";
		}
		else if(strVal == "Percent of White") {
			res = "white_or10";
		}
		else if(strVal == "Percent of Black") {
			res = "bl_or10";
		}
		else if(strVal == "Percent of American Indian") {
			res = "aian_or10";
		}
		else if(strVal == "Percent of Asian") {
			res = "asia_or10";
		}
		else if(strVal == "Percent of Some Other Race") {
			res = "somoth_or1";
		}
		else if(strVal == "Percent of Fulton Housing Units") {
			res = "tothu10";
		}
		else if(strVal == "Percent of Total Occupied Units") {
			res = "totoccu_10";
		}
		else if(strVal == "Percent of Total Vacant Units") {
			res = "totvach_10";
		}

		return res;
	}

	function colormap(d) { 
		return parseInt(d.properties.STATEFP10); 
	}

	function wrap(text, width) {
		text.each(function() {
			var text = d3.select(this),
			    words = text.text().split(/\s+/).reverse(),
			    word,
			    line = [],
			    lineNumber = 0,
			    lineHeight = 1.1, // ems
			    y = text.attr("y"),
			    dy = parseFloat(text.attr("dy")),
			    tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
			while (word = words.pop()) {
				line.push(word);
				tspan.text(line.join(" "));
				if (tspan.node().getComputedTextLength() > width) {
					line.pop();
					tspan.text(line.join(" "));
					line = [word];
					tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
				}
			}
		});
	}

	function getLabel(indicator) {
		var textTick = {};
			
		if (indicator == 'totpop10') {
			textTick = { tickFormat: d3.format(",.1"), indicatorName: 'Percent of Fulton Population'};
		} 
		else if (indicator == 'oner_10') {
			textTick = { tickFormat: d3.format(",.1"), indicatorName: 'Percent of One Race Total'};
		} 
		else if (indicator == 'white_or10') {
			textTick = { tickFormat: d3.format(",.1"), indicatorName: 'Percent of White'};
		} 
		else if (indicator == 'bl_or10') {
			textTick = { tickFormat: d3.format(",.1"), indicatorName: 'Percent of Black'};
		} 
		else if (indicator == 'aian_or10') {
			textTick = { tickFormat: d3.format(",.1"), indicatorName: 'Percent of American Indian'};
		} 
		else if (indicator == 'asia_or10') {
			textTick = { tickFormat: d3.format(",.1"), indicatorName: 'Percent of Asian'};
		} 
		else if (indicator == 'somoth_or1') {
			textTick = { tickFormat: d3.format(",.1"), indicatorName: 'Percent of Some Other Race'};
		} 
		else if (indicator == 'tothu10') {
			textTick = { tickFormat: d3.format(",.1"), indicatorName: 'Percent of Fulton Housing Units'};
		} 
		else if (indicator == 'totoccu_10') {
			textTick = { tickFormat: d3.format(",.1"), indicatorName: 'Percent of Total Occupied Units'};
		} 
		else if (indicator == 'totvach_10') {
			textTick = { tickFormat: d3.format(",.2"), indicatorName: 'Percent of Total Vacant Units'};
		} 
		else {

		}
		
		return textTick;
	}
		
	function highlightCountry(name, data) {
		highlightedCity = name;
		updateColor(data);
	}

	function updateColor(data) {
		var fieldID = "totpop10";
		var val;

		mink = d3.min(data, function(d) { 
			return d.properties[fieldID]/totalPop;
			});
	    maxk = d3.max(data, function(d) {
			return d.properties[fieldID]/totalPop;
		});

		mapChart.selectAll("path.land")
		   .classed("highlighted", false)
		   .classed("selected", false)
		   .transition()
		   .duration(50)
		   .attr("fill",function(d) {
		   		if (highlightedCity && d.properties.NAME.toUpperCase() == highlightedCity.toUpperCase()) {
		   			return "#FCB12E";
		   		}
		   		else {
		   			val = d.properties[fieldID];

					migrationsColor.domain([mink+0.1, maxk+0.1]); 
					val = val/totalPop+0.1;
			    	return migrationsColor(val);
		   		}
			})
			.attr("stroke", "white");
	}

	function selectBar(code) {
		if (selectedBar == code) {
			selectedBar = null;
		} 
		else {
			selectedBar = code;
		}

		updateBar(selectedBar);
	}

	function updateBar(code) {
		if (datasetIndicators != null) {
			var svg = d3.select("#barChart svg .mainWrapper");
			var fieldID =  textToID($(".barTab #dd-text").text());
			svg.selectAll("rect")
			.style("fill", function(d) {
				if (code != null || selectedBar != null) {
					var correctCode = code || selectedBar;
					var cityName = countryNamesByCode[correctCode];

					if (cityName.toUpperCase() == d.name.toUpperCase()) {
						return "#FCB12E";
					}
					else {
						if (fieldID=='totpop10') {
							currentvalue = d.value/d.populationTotals;
						}
						else if (fieldID=='oner_10'|| fieldID=='white_or10'||
							fieldID=='bl_or10'||fieldID=='aian_or10'||
							fieldID=='asia_or10'||fieldID=='somoth_or1') {
							currentvalue = d.value/d.totpop10;
						}
						else if (fieldID=='tothu10') {	
							currentvalue = d.value/d.housingTotals;
						}
						else if (fieldID=='totoccu_10'||fieldID=='totvach_10') {
							currentvalue = d.value/d.housing;
						}
						else {  
							currentvalue = d.value;
						}

						return colorScale(currentvalue);
					}
				}
				else {
					if (fieldID=='totpop10') {
						currentvalue = d.value/d.populationTotals;
					}
					else if (fieldID=='oner_10'|| fieldID=='white_or10'||
						fieldID=='bl_or10'||fieldID=='aian_or10'||
						fieldID=='asia_or10'||fieldID=='somoth_or1') {
						currentvalue = d.value/d.totpop10;
					}
					else if (fieldID=='tothu10') {	
						currentvalue = d.value/d.housingTotals;
					}
					else if (fieldID=='totoccu_10'||fieldID=='totvach_10') {
						currentvalue = d.value/d.housing;
					}
					else {  
						currentvalue = d.value;
					}

					return colorScale(currentvalue);
				}
			})
		}
	}

	function updateDetails() {
		var countryName, totalPopulation, numMigrants;

		if (highlightedCity != null) {
			countryName = highlightedCity;
			var iso3 = countryCodeByName[highlightedCity.toUpperCase()];
			var countryRem = totalPopArr[iso3];
			totalPopulation = (countryRem != null ? str2num(countryRem) : NaN);
			numMigrants = housingTotalsbycity[iso3];
			totalAid = occuTotalsbycity[iso3];
			totalvacant = vacantTotalsbycity[iso3];
		} 
		else {
			totalPopulation = totalPop;
			numMigrants = housingTotals;
			totalvacant = vacantTotals;
			totalAid = occuTotals;
		}

		if (highlightedCity)
			$("#cityText_Sec").text(countryName);
		else if(highlightedCity == null)
			$("#cityText_Sec").text("Fulton County");
		
		$("#text_1_1_2_1").text(numberFormat(totalPopulation)); // population
		$("#text_1_2_2_1").text(numberFormat(totalAid)); // occupied
		$("#text_2_1_2_1").text(numberFormat(numMigrants)); // housing units
		$("#text_2_2_2_1").text(numberFormat(totalvacant)); // vacant housing units
	}

	function calcTotalsByCounty(values, index) {
		var totals, i, yi, countryData, y, val, max = NaN;

	  	for (i=0; i<values.length; i++) {
	    	countryData = values[i];
	      	val = str2num(countryData[index]);
	      	if (!isNaN(val)) {
	        	if (totals === undefined) totals = 0;
	        	totals += val;
	      	}
	  	}

	  	return totals;
	}

	function str2num(str) {
	  // empty string gives 0 when using + to convert
	  if (str === null || str === undefined || str.length == 0) return NaN;
	  return +str;
	}

	var percentFormat = function(v) {
	  var percent_fmt = d3.format(",.1%");
	  return percent_fmt(v);
	};

	var spercentFormat = function(v) {
	  var spercent_fmt = d3.format(",.1%");
	  return spercent_fmt(v);
	};

	function pickIndicatorData(dataset, indicator) {
		var populationTotals = calcTotalsByCounty(dataset,'totpop10');
		var housingTotals = calcTotalsByCounty(dataset,'tothu10');
			
		$.each(dataset, function(i,d) {	
			d.value = d[indicator];	
			d.totpop10 = d['totpop10'];
			d.housing = d['tothu10']
			d.populationTotals = populationTotals;
			d.housingTotals = housingTotals;
		});
		return dataset;
	}
	
	function stringParser(strArr) {
		var res = "";
		for (i = 2; i < strArr.length; i++) {
			var temp = strArr[i];
			if (i < strArr.length-1)
				temp = temp.concat(" "); 
    		res = res.concat(temp);
		}
		return res;
	}

	function truncateString(strArr) {
		var res = "";
		for (i=0; i<strArr.length-1; i++) {
			var temp = strArr[i];
			if (i < strArr.length-2)
				temp = temp.concat(" ");
			res = res.concat(temp);
		}
		return res;
	}

	var origindata;
	
	d3.json("data/county_plotchart.json", function(data) { 	
		origindata = data;
		datasetIndicators = origindata;
		// pick initial indicator
		var indicator = textToID($(".barTab #dd-text").text());
		// pick initial order, set indicator to d.value and order for plotting
		var filteredDataByIndicator = pickIndicatorData(datasetIndicators,indicator);
		// get text and ticks
		var textTick = getLabel(indicator);
		// create bar chart
		createBarChart(svgContainerBarChart, filteredDataByIndicator, textTick);
	});

	function createBarChart(svgContainer, datasetIndicators, textTick) {
		//set up container for mouseover interaction
		var widthBarChart = $("#mapThirdRow").width() - widthOffset; // hack for getting the width from the active tab
		var heightBarChart = $("#barSecondRow").height() - heightOffset;	

		var x = d3.scale.ordinal().rangeRoundBands([0, widthBarChart], .2);
		var y = d3.scale.linear().range([heightBarChart-1, 50]);	// -1 offset is used to make sure x axis line stays inside the wrapperbar
		var xAxis = d3.svg.axis().scale(x).orient("bottom").tickSize(0,0);

		var yAxis = d3.svg.axis().scale(y).orient("left").tickFormat(spercentFormat).tickSize(-widthBarChart,0).ticks(4);

		var svg = svgContainer.append("g").attr("class", "mainWrapper")
	    	.attr("transform", "translate(35,-55)");
					
	    x.domain(datasetIndicators.map(function(d) { 
	    	var arr = d.name.split(" ");
	    	var shortStr = truncateString(arr);
	    	return shortStr; 
	    }));
		
		//set y domain from zero to d.max only if d.min is above zero
		if (d3.min(datasetIndicators, function(d) { return d.value; }) > 0 ) {
			var min = 0;
		} else {
			var min = d3.min(datasetIndicators, function(d) { return d.value; });
		}
		
		var max = d3.max(datasetIndicators, function(d){ return parseInt(d.value)})
	    var currentvalue;
		var fieldID =  textToID($(".barTab #dd-text").text());
		if (fieldID=='totpop10') {
			mink = d3.min(datasetIndicators, function(d) {return d.value/d.populationTotals});
			maxk = d3.max(datasetIndicators, function(d) {return d.value/d.populationTotals});
		}
		else if (fieldID=='oner_10'|| fieldID=='white_or10'||
			fieldID=='bl_or10'||fieldID=='aian_or10'||fieldID=='asia_or10'||
			fieldID=='somoth_or1') {
			mink = d3.min(datasetIndicators, function(d) {return d.value/d.totpop10});
			maxk = d3.max(datasetIndicators, function(d) {return d.value/d.totpop10});
		}
		else if (fieldID=='tothu10') {
			mink = d3.min(datasetIndicators, function(d) {return d.value/d.housingTotals});
			maxk = d3.max(datasetIndicators, function(d) {return d.value/d.housingTotals});
		}
		else if (fieldID=='totoccu_10'||fieldID=='totvach_10'){
			mink = d3.min(datasetIndicators, function(d) {return d.value/d.housing});
			maxk = d3.max(datasetIndicators, function(d) {return d.value/d.housing});
		}
		else{
			mink = min;
			maxk = max;
		}
			    
		y.domain([mink, maxk]);
		colorScale.domain([mink, maxk]);

	    svg.append("g")
		.attr("class", "y axis")
		.call(yAxis)
		.call(function(selections) {
	  		selections.selectAll('.tick text')
	  		.attr('x',-5)
	        .attr('fill','#8F8F8F')
	        .attr('font-size','11px')
	        .attr('font-family','Roboto');
	  	})
		.append("text")
		.attr("class", "axisText")
		.attr("transform", "rotate(-90)")
		.attr("y", 1)
		.attr("x", 40)
		.attr("dy", ".71em")
		.style("text-anchor", "end")
		.style("fill", "red");

		svgContainer.select(".y")
		// .transition().duration(1500)
	    .call(yAxis);
			
	    svg.selectAll(".bar")
	    .data(datasetIndicators)
		.enter().append("rect")
		.style("fill", function(d) {  
			if (fieldID=='totpop10') {
				currentvalue = d.value/d.populationTotals;
			}
			else if (fieldID=='oner_10'|| fieldID=='white_or10'||
				fieldID=='bl_or10'||fieldID=='aian_or10'||
				fieldID=='asia_or10'||fieldID=='somoth_or1') {
				currentvalue = d.value/d.totpop10;
			}
			else if (fieldID=='tothu10') {	
				currentvalue = d.value/d.housingTotals;
			}
			else if (fieldID=='totoccu_10'||fieldID=='totvach_10') {
				currentvalue = d.value/d.housing;
			}
			else {  
				currentvalue = d.value;
			}

			return colorScale(currentvalue);
		})
	    .attr("x", function(d, i) { 
	    	return x(truncateString(d.name.split(" ")))+xOffset; 
	    })
	    .attr("width", x.rangeBand()-7)
	    .attr("y", function(d, i) {
			if (fieldID=='totpop10') {
				currentvalue = d.value/d.populationTotals;
			}
			else if (fieldID=='oner_10'|| fieldID=='white_or10'||
				fieldID=='bl_or10'||fieldID=='aian_or10'||
				fieldID=='asia_or10'||fieldID=='somoth_or1') {
				currentvalue = d.value/d.totpop10;
			}
			else if (fieldID=='tothu10') {
				currentvalue = d.value/d.housingTotals;
			}
			else if (fieldID=='totoccu_10'||fieldID=='totvach_10'){
				currentvalue = d.value/d.housing;
			}
			else {  
				currentvalue = d.value;
			}
			return y(Math.max(0,currentvalue));
		})
	    .attr("height", function(d) { 
			if (fieldID=='totpop10'){
				currentvalue = d.value/d.populationTotals;
			}
			else if (fieldID=='oner_10'|| fieldID=='white_or10'||
				fieldID=='bl_or10'||fieldID=='aian_or10'||
				fieldID=='asia_or10'||fieldID=='somoth_or1'){
				currentvalue = d.value/d.totpop10;
			}
			else if (fieldID=='tothu10'){
				currentvalue = d.value/d.housingTotals;
			}
			else if (fieldID=='totoccu_10'||fieldID=='totvach_10'){
				currentvalue = d.value/d.housing;
			}
			else {  
			  currentvalue = d.value;
			}

			return Math.abs(y(currentvalue) - y(0)); 
		})
		.on("mouseover", function(d) {	// set up on mouseover events
			div.transition().duration(250).style("opacity", 0.7);
			var coordinates = d3.mouse(this);
			var x = coordinates[0];
			var y = coordinates[1];
			div.html(
					'<p id=cityName>' + d.name + '</p>' +
					'<p id=newLine>' + "<br>" + '</p>' +
					'<p id=valLine>' + percentFormat(currentvalue) + '</p>' +
					'<p id=descLine>' + textTick.indicatorName + '</p>'		
				)  
		        .style("left", (x + 100) + "px")     
		        .style("top", (y - 80) + "px");

			if (selectedBar == null) {
				highlightCountry(d.name, features); 
				updateDetails();

				d3.select(this).style("fill","#FCB12E");
				
				if (fieldID=='totpop10') {
					currentvalue = d.value/d.populationTotals;
				}
				else if (fieldID=='oner_10'|| fieldID=='white_or10'||
				  	fieldID=='bl_or10'||fieldID=='aian_or10'||
				  	fieldID=='asia_or10'||fieldID=='somoth_or1') {
					currentvalue = d.value/d.totpop10;
				}
				else if (fieldID=='tothu10') {
					currentvalue = d.value/d.housingTotals;
				}
				else if (fieldID=='totoccu_10'||fieldID=='totvach_10') {
					currentvalue = d.value/d.housing;
				}
				else {  
					currentvalue = d.value;
				} 
			}			
		})
		.on("click", function(d) { 
			var geoID = countryCodeByName[d.name.toUpperCase()];
        	selectBar(geoID);
        	if (selectedBar == null)
        		highlightCountry(null, features);
        	else
        		highlightCountry(d.name, features);
			updateDetails();
        })
		.on("mouseout", function(d) {
			div.transition().duration(250).style("opacity", 1e-6);
			if (selectedBar == null) {
				highlightCountry(null, features); 
				updateDetails();

				d3.select(this).style("fill", function(d) {
					if (fieldID=='totpop10') {
						currentvalue = d.value/d.populationTotals;
					}
					else if (fieldID=='oner_10'|| fieldID=='white_or10'||
						fieldID=='bl_or10'||fieldID=='aian_or10'||
						fieldID=='asia_or10'||fieldID=='somoth_or1') {
						currentvalue = d.value/d.totpop10;
					}
					else if (fieldID=='tothu10') {	
						currentvalue = d.value/d.housingTotals;
					}
					else if (fieldID=='totoccu_10'||fieldID=='totvach_10') {
						currentvalue = d.value/d.housing;
					}
					else {  
						currentvalue = d.value;
					}

					return colorScale(currentvalue);
				});
			}
			
		});

		svg.append("g")
		.attr("class", "x axis")
		.call(xAxis)
		.call(function(selections) {
	  		selections.selectAll('.tick text')
	        .attr('transform', 'translate(-15,13) rotate(315)')
	        .attr('fill','#AEAEAE')
	        .attr('font-size','9px')
	        .attr('font-family','Roboto');
	        selections.selectAll('.tick line')
	        .attr('y2','0');
	  	})
	  	.attr("transform", "translate(0," + heightBarChart + ")")
	  	.selectAll(".tick text")
      	.call(wrap, x.rangeBand());
			
		// print "no data" if dataset max and min = 0
		if ((d3.min(datasetIndicators, function(d) { return d.value; }) == 0 ) && (d3.max(datasetIndicators, function(d) { return d.value; }) == 0 )) {
			svg.append("g")
			.attr("class", "noData")
			.append("text")
			.attr("x", widthBarChart)
			.attr("y", heightBarChart)
			.text("No Data Available");
		}
			
		// $(window).resize(function() {
		// 	d3.select("svg").style("width", $(window).width() + margin.left + margin.right- 400+"px")
		// 	.style("height", $(window).height() - margin.top - margin.bottom + margin.top + margin.bottom- 80+"px");
		// });
			
		// maplegend.style("right",15+"px").style("bottom",100+"px").style("position","absolute");
			
		// $(window).resize(function() {
		// 	maplegend.style("right",15+"px").style("bottom",100+"px").style("position","absolute");
		// });
	}
  
	function updateBarChartData(indicator) {	
		// set indicator to d.value and order for plotting
		var filteredDataByIndicator = pickIndicatorData(datasetIndicators,indicator);
		// get text and ticks
		var textTick = getLabel(indicator);
		// create bar chart
		updateBarChart(svgContainerBarChart, filteredDataByIndicator, textTick);
	}

	function updateBarChart(svgContainer, datasetIndicators, textTick) {	
		var noData = svgContainer.select(".noData");
		if (!noData) {
			// do nothing
		} else {
			noData.remove();
		}

		//set up container for mouseover interaction
		var div = d3.select("#barCol .pinwheelTooltip");
		// var margin = {top: 19.5, right: 19.5, bottom: 19.5, left: 89.5};

		var widthBarChart = $("#barSecondRow").width() - widthOffset;
		var heightBarChart = $("#barSecondRow").height() - heightOffset;

		var x = d3.scale.ordinal().rangeRoundBands([0, widthBarChart], .2);
		var xAxis = d3.svg.axis().scale(x).orient("bottom").tickSize(0,0);

		var y = d3.scale.linear().range([heightBarChart-1, 50]); // -1 offset is used to make sure x axis line stays inside the wrapper
		var yAxis = d3.svg.axis().scale(y).orient("left").tickFormat(spercentFormat).tickSize(-widthBarChart,0).ticks(4);
							
	    x.domain(datasetIndicators.map(function(d) {
	    	var arr = d.name.split(" ");
	    	var shortStr = truncateString(arr);
	    	return shortStr;
	    }));

		//set y domain from zero to d.max only if d.min is above zero
		if (d3.min(datasetIndicators, function(d) {return d.value;})>0) {
			var min = 0;
		} else {
			var min = d3.min(datasetIndicators, function(d) {return d.value;});
		}
		
		var max = d3.max(datasetIndicators, function(d){return parseInt(d.value)})
	    var currentvalue;
		var fieldID = textToID($(".barTab #dd-text").text());
		if (fieldID=='totpop10') {
			mink = d3.min(datasetIndicators, function(d) { return d.value/d.populationTotals});
			maxk = d3.max(datasetIndicators, function(d) { return d.value/d.populationTotals});
		}
		else if (fieldID=='oner_10'|| fieldID=='white_or10'||
			fieldID=='bl_or10'||fieldID=='aian_or10'||
			fieldID=='asia_or10'||fieldID=='somoth_or1') {
			mink = d3.min(datasetIndicators, function(d) { return d.value/d.totpop10 });
			maxk = d3.max(datasetIndicators, function(d) { return d.value/d.totpop10 });
		}
		else if (fieldID=='tothu10') {
			mink = d3.min(datasetIndicators, function(d) { return d.value/d.housingTotals});
			maxk = d3.max(datasetIndicators, function(d) { return d.value/d.housingTotals});
		}
		else if (fieldID=='totoccu_10'||fieldID=='totvach_10') {
			mink = d3.min(datasetIndicators, function(d) { return d.value/d.housing});
			maxk = d3.max(datasetIndicators, function(d) { return d.value/d.housing});
		}
		else {
			mink = min;
			maxk = max;
		}
			  
		colorScale.domain([mink, maxk]);
		y.domain([0, maxk]);

		var svg = svgContainer.select(".mainWrapper");

	    svgContainer.select(".y")
	    .call(yAxis)
	    .call(function(selections) {
	  		selections.selectAll('.tick text')
	  		.attr('x',-5)
	        .attr('fill','#8F8F8F')
	        .attr('font-size','11px')
	        .attr('font-family','Roboto');
	  	});

	    var rect = svg.selectAll("rect").data(datasetIndicators, function(d) { 
	    	var arr = d.name.split(" ");
	    	var shortStr = truncateString(arr);
	    	return shortStr;
	    });
		
		rect.enter().append("rect")
		.style("fill", function(d) { 
			if (fieldID=='totpop10') {
				currentvalue = d.value/d.populationTotals;
			}
			else if (fieldID=='oner_10'|| fieldID=='white_or10'||
				fieldID=='bl_or10'||fieldID=='aian_or10'||
				fieldID=='asia_or10'||fieldID=='somoth_or1') {
				currentvalue = d.value/d.totpop10;
			}
			else if (fieldID=='tothu10') {	
				currentvalue = d.value/d.housingTotals;
			}
			else if (fieldID=='totoccu_10'||fieldID=='totvach_10') {
				currentvalue = d.value/d.housing;
			}
			else {  
				currentvalue = d.value;
			}

			return colorScale(currentvalue);
		})
	    .attr("x", function(d, i) { 
	    	return x(truncateString(d.name.split(" ")))+xOffset; 
	    })
	    .attr("width", x.rangeBand()-7)
	    .attr("y", function(d, i) { return y(Math.max(0, d.value)); })
	    .attr("height", 0 );
					
		rect.transition().duration(500)
		.style("fill", function(d) { 
			if (fieldID=='totpop10') {
				currentvalue = d.value/d.populationTotals;
			}
			else if (fieldID=='oner_10'|| fieldID=='white_or10'||
				fieldID=='bl_or10'||fieldID=='aian_or10'||
				fieldID=='asia_or10'||fieldID=='somoth_or1') {
				currentvalue = d.value/d.totpop10;
			}
			else if (fieldID=='tothu10') {	
				currentvalue = d.value/d.housingTotals;
			}
			else if (fieldID=='totoccu_10'||fieldID=='totvach_10') {
				currentvalue = d.value/d.housing;
			}
			else {  
				currentvalue = d.value
			}

			return colorScale(currentvalue);
		})
	    .attr("x", function(d) { 
	    	return x(truncateString(d.name.split(" ")))+xOffset; 
	    })
	    .attr("width", x.rangeBand()-7)
	    .attr("y", function(d, i) { 
			if (fieldID=='totpop10') {
				currentvalue = d.value/d.populationTotals;
			}
			else if (fieldID=='oner_10'|| fieldID=='white_or10'||
				fieldID=='bl_or10'||fieldID=='aian_or10'||
				fieldID=='asia_or10'||fieldID=='somoth_or1') {
				currentvalue = d.value/d.totpop10;
			}
			else if (fieldID=='tothu10') {
				currentvalue = d.value/d.housingTotals;
			}
			else if (fieldID=='totoccu_10'||fieldID=='totvach_10') {
				currentvalue = d.value/d.housing;
			}
			else {	  
				currentvalue = d.value
			}
			  
			return y(Math.max(0, currentvalue)); 
		})
	    .attr("height", function(d) { 
			if (fieldID=='totpop10') {
				currentvalue = d.value/d.populationTotals;
			}
			else if (fieldID=='oner_10'|| fieldID=='white_or10'||
				fieldID=='bl_or10'||fieldID=='aian_or10'||
				fieldID=='asia_or10'||fieldID=='somoth_or1') {
				currentvalue = d.value/d.totpop10;
			}
			else if (fieldID=='tothu10') {	
				currentvalue = d.value/d.housingTotals;
			}
			else if (fieldID=='totoccu_10'||fieldID=='totvach_10') {
				currentvalue = d.value/d.housing;
			}
			else {  
				currentvalue = d.value
			}

			return Math.abs(y(currentvalue) - y(0)); 
		});
			
		rect.exit().transition().duration(1500).attr("height",0).remove();
			
		// set up on mouseover events
		rect.on("mouseover", function(d) {
			div.transition().duration(250).style("opacity", 0.7);
			var coordinates = d3.mouse(this);
			var x = coordinates[0];
			var y = coordinates[1];
			div.html(
					'<p id=cityName>' + d.name + '</p>' +
					'<p id=newLine>' + "<br>" + '</p>' +
					'<p id=valLine>' + percentFormat(currentvalue) + '</p>' +
					'<p id=descLine>' + textTick.indicatorName + '</p>'		
				)  
		        .style("left", (x + 100) + "px")     
		        .style("top", (y - 80) + "px");

			if (selectedBar == null) {
				highlightCountry(d.name, features); 
				updateDetails();

				d3.select(this).style("fill","#FCB12E");
		    
				if (fieldID=='totpop10') {
					currentvalue = d.value/d.populationTotals;
				}
				else if (fieldID=='oner_10'|| fieldID=='white_or10'||
					fieldID=='bl_or10'||fieldID=='aian_or10'||
					fieldID=='asia_or10'||fieldID=='somoth_or1') {
					currentvalue = d.value/d.totpop10;
				}
				else if (fieldID=='tothu10') {
					currentvalue = d.value/d.housingTotals;
				}
				else if (fieldID=='totoccu_10'||fieldID=='totvach_10') {
					currentvalue = d.value/d.housing;
				}
				else {
					currentvalue = d.value
				}
			}						
		})
		.on("click", function(d) {
        	var geoID = countryCodeByName[d.name.toUpperCase()];
        	selectBar(geoID);
        	if (selectedBar == null)
        		highlightCountry(null, features);
        	else
        		highlightCountry(d.name, features);
			updateDetails();
        })
		.on("mouseout", function(d) {
			div.transition().duration(250).style("opacity", 1e-6);
			if (selectedBar == null) {
				highlightCountry(null, features);
				updateDetails();

				d3.select(this).style("fill", function(d) { 
					if (fieldID=='totpop10') {
						currentvalue = d.value/d.populationTotals;
					}
					else if (fieldID=='oner_10'|| fieldID=='white_or10'||
						fieldID=='bl_or10'||fieldID=='aian_or10'||
						fieldID=='asia_or10'||fieldID=='somoth_or1') {
						currentvalue = d.value/d.totpop10;
					}
					else if (fieldID=='tothu10') {	
						currentvalue = d.value/d.housingTotals;
					}
					else if (fieldID=='totoccu_10'||fieldID=='totvach_10') {
						currentvalue = d.value/d.housing;
					}
					else {  
						currentvalue = d.value
					}

					return colorScale(currentvalue);
				});
			}

		});

	   	svgContainer.select(".x.axis")
	   	.call(xAxis)
		.call(function(selections) {
	  		selections.selectAll('.tick text')
	        .attr('transform', 'translate(-15,13) rotate(315)')
	        .attr('fill','#AEAEAE')
	        .attr('font-size','10px')
	        .attr('font-family','Roboto');
	        selections.selectAll('.tick line')
	        .attr('y2','0');
	  	})
	  	.attr("width",400)
		.selectAll(".tick text")
      	.call(wrap, x.rangeBand());
			
		// print "no data" if dataset max and min = 0
		if ((d3.min(datasetIndicators, function(d) { return d.value; }) == 0 ) && (d3.max(datasetIndicators, function(d) { return d.value; }) == 0 )) {
			svgContainer.append("g")
			.attr("class", "noData")
			.append("text")
			.attr("x", widthBarChart/2.3)
			.attr("y", heightBarChart/2)
			.attr("font-size", "36px")
			.attr("fill", "#6D6E70")
			.text("No Data Available");
		}
	} // close update bar chart function
});