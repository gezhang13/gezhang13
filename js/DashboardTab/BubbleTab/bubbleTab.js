$(document).ready(function() { 

// Variables 
    var SecondRowheight = $('.well').height()*0.8;
	var SecondRowwidth = $('.well').width();
	
	var margin = {top: -20, right: 10, bottom: 10, left: 100};
	var xScale = d3.scale.linear().range([0, SecondRowwidth-250]),
		yScale = d3.scale.linear().range([SecondRowheight, 30]),
		radiusScale = d3.scale.sqrt().range([0, 40]);
		
// Initialize elements
	d3.select("#tab3")
	  .append("div")
	  .attr("id","bubbleSecondRow")
	  .style({'width':SecondRowwidth+'px','height':SecondRowheight+'px', 'margin-left':'15px',"margin-top":'20px'});

	d3.select("#tab3")
	  .append("div")
	  .attr("id","bubbleThirdRow")
	  .style({'width':SecondRowwidth+'px','height':40+'px', 'margin-left':'15px',"margin-top":'20px'});
	  
	var bubblesvg = d3.select("#bubbleSecondRow")
					  .append("svg")
					  .attr("width", SecondRowwidth-100)
					  .attr("height",SecondRowheight+30)
					  .append("g")
					  .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		
					  
	var tooltip = d3.select("#bubbleSecondRow")
					.append("div")
					.attr("class", "pinwheelTooltip")
					.style("opacity", 0);
		
	var zfirstlevel = d3.select("#mapDropDown4")
						.append("div")
					    .attr("class","btn-group")
					    .attr("id","dropDownGrp")
							  
	zfirstlevel.append("button")
			   .attr("class","btn btn-default dropdown-text")
			   .attr("id","dd-button")
			   .append("p")
			   .attr("id","dd-text")
			   .text("Percent of Fulton Population")
			   			   
	zfirstlevel.append("button")
			   .attr("class","btn btn-default dropdown-toggle")
			   .attr("id","dropDownSide")
			   .attr("data-toggle","dropdown")
			   .attr("aria-haspopup","true")
			   .attr("aria-expanded","true")
			   .append("span")
			   .attr("class","caret")
			   
	zfirstlevel.append("div")
			   .attr("class","dropdown-menu")
			   .attr("id","menu45")
			   .html("<li><a class='dropdown-item' href='#'>Percent of Fulton Population</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of One Race Total</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of White</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of Black</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of American Indian</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of Asian</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of Some Other Race</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of Fulton Housing Units</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of Total Occupied Units</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of Total Vacant Units</a></li>"
					)

	var yfirstlevel = d3.select("#bubbleSecondRow")
						.append("div")
					    .attr("class","btn-group")
					    .attr("id","dropDownGrp")
						.style({"top":"50%","position":"absolute","-webkit-transform": "rotate(-90deg)","left":"-10%"})
							  
	yfirstlevel.append("button")
			   .attr("class","btn btn-default dropdown-text")
			   .attr("id","dd-button")
			   .append("p")
			   .attr("id","dd-text")
			   .text("Percent of Fulton Population")
			   			   
	yfirstlevel.append("button")
			   .attr("class","btn btn-default dropdown-toggle")
			   .attr("id","dropDownSide")
			   .attr("data-toggle","dropdown")
			   .attr("aria-haspopup","true")
			   .attr("aria-expanded","true")
			   .append("span")
			   .attr("class","caret")
			   
	yfirstlevel.append("div")
			   .attr("class","dropdown-menu")
			   .attr("id","menu45")
			   .style("-webkit-transform", "rotate(90deg)")
			   .html("<li><a class='dropdown-item' href='#'>Percent of Fulton Population</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of One Race Total</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of White</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of Black</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of American Indian</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of Asian</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of Some Other Race</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of Fulton Housing Units</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of Total Occupied Units</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of Total Vacant Units</a></li>"
					)
					
					
	var xfirstlevel = d3.select("#bubbleThirdRow")
						.append("div")
					    .attr("class","btn-group dropup")
					    .attr("id","dropDownGrp")
						.style({"left":"30%","position":"absolute"})
							  
	xfirstlevel.append("button")
			   .attr("class","btn btn-default dropdown-text")
			   .attr("id","dd-button")
			   .append("p")
			   .attr("id","dd-text")
			   .text("Percent of Fulton Population")
			   			   
	xfirstlevel.append("button")
			   .attr("class","btn btn-default dropdown-toggle")
			   .attr("id","dropDownSide")
			   .attr("data-toggle","dropdown")
			   .attr("aria-haspopup","true")
			   .attr("aria-expanded","true")
			   .append("span")
			   .attr("class","caret")
			   
	xfirstlevel.append("div")
			   .attr("class","dropdown-menu")
			   .attr("id","menu45")
			   .html("<li><a class='dropdown-item' href='#'>Percent of Fulton Population</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of One Race Total</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of White</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of Black</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of American Indian</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of Asian</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of Some Other Race</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of Fulton Housing Units</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of Total Occupied Units</a></li>"+
					"<li><a class='dropdown-item' href='#'>Percent of Total Vacant Units</a></li>"
					)

	var bubbledot = bubblesvg.append("g")
							 .attr("class", "dots");


// functions 
	
	
	function UpdateDataByIndexDropDownMenu(dataset,indexx,indexy,indexz){
		if (indexx=='totpop10') {
			$.each(dataset, function( i, d ) {	
				d.x = d.x/d.populationTotals;		
			});
		}
		else if (indexx=='oner_10'|| indexx=='white_or10'||
			indexx=='bl_or10'||indexx=='aian_or10'||
			indexx=='asia_or10'||indexx=='somoth_or1') {
			$.each(dataset, function( i, d ) {	
				d.x = d.x/d.totpop10;		
			});	
		}
		else if (indexx=='tothu10') {
			$.each(dataset, function( i, d ) {	
				d.x = d.x/d.housingTotals;		
			});
		}
		else if (indexx=='totoccu_10'||indexx=='totvach_10') {
			$.each(dataset, function( i, d ) {	
				d.x = d.x/d.tothu10;		
			});
		}
		else {
			$.each(dataset, function( i, d ) {	
				d.x = d.x;		
			});
		}
		
		if (indexy=='totpop10') {
			$.each(dataset, function( i, d ) {	
				d.y = d.y/d.populationTotals;		
			});
		}
		else if (indexy=='oner_10'|| indexy=='white_or10'||
			indexy=='bl_or10'||indexy=='aian_or10'||
			indexy=='asia_or10'||indexy=='somoth_or1') {
			$.each(dataset, function( i, d ) {	
				d.y = d.y/d.totpop10;		
			});	
		}
		else if (indexy=='tothu10') {
			$.each(dataset, function( i, d ) {	
				d.y = d.y/d.housingTotals;		
			});
		}
		else if (indexy=='totoccu_10'||indexy=='totvach_10') {
			$.each(dataset, function( i, d ) {	
				d.y = d.y/d.tothu10;		
			});
		}
		else {
			$.each(dataset, function( i, d ) {	
				d.y = d.y;		
			});
		}

		if (indexz=='totpop10') {
			$.each(dataset, function( i, d ) {	
				d.z = d.z/d.populationTotals;		
			});
		}
		else if (indexz=='oner_10'|| indexz=='white_or10'||
			indexz=='bl_or10'||indexz=='aian_or10'||
			indexz=='asia_or10'||indexz=='somoth_or1') {
			$.each(dataset, function( i, d ) {	
				d.z = d.z/d.totpop10;		
			});	
		}
		else if (indexz=='tothu10') {
			$.each(dataset, function( i, d ) {	
				d.z = d.z/d.housingTotals;		
			});
		}
		else if (indexz=='totoccu_10'||indexz=='totvach_10') {
			$.each(dataset, function( i, d ) {	
				d.z = d.z/d.tothu10;		
			});
		}
		else {
			$.each(dataset, function( i, d ) {	
				d.z = d.z;		
			});
		}		
			 
		return dataset;
	}
	
	function x(d) { return d.x; }
	function y(d) { return d.y; }
	function radius(d) { return d.z; }
	function key(d) { return d.name; }

	function textToID(strVal) {
		var res = "";
		if (strVal == "Percent of Fulton Population") {
			res = "totpop10";
		}
		else if(strVal == "Percent of One Race Total") {
			res = "oner_10";
		}
		else if(strVal == "Percent of White") {
			res = "white_or10";
		}
		else if(strVal == "Percent of Black") {
			res = "bl_or10";
		}
		else if(strVal == "Percent of American Indian") {
			res = "aian_or10";
		}
		else if(strVal == "Percent of Asian") {
			res = "asia_or10";
		}
		else if(strVal == "Percent of Some Other Race") {
			res = "somoth_or1";
		}
		else if(strVal == "Percent of Fulton Housing Units") {
			res = "tothu10";
		}
		else if(strVal == "Percent of Total Occupied Units") {
			res = "totoccu_10";
		}
		else if(strVal == "Percent of Total Vacant Units") {
			res = "totvach_10";
		}
		return res;
	}
	
	function GetLabel(indicator){
	var textTick = {};
		
		if (indicator == 1) {
			textTick = { text: 'Number', tickFormat: d3.format(",.1"), tableClass: 'economy', indicatorName: 'Total Population', accessorFunction: function(d) {return d.totpop10;}, index: 'No', scored: 'Positive'};
		} else if (indicator == 2) {
			textTick = { text: 'Number', tickFormat: d3.format(",.1"), tableClass: 'economy', indicatorName: 'One Race Total', accessorFunction: function(d) {return d.oner_10;}, index: 'No', scored: 'Positive'};
		} else if (indicator == 3) {
			textTick = { text: 'Number', tickFormat: d3.format(",.1"), tableClass: 'economy', indicatorName: 'White', accessorFunction: function(d) {return d.white_or10;}, index: 'No', scored: 'Positive'};
		} else if (indicator == 4) {
			textTick = { text: 'Number', tickFormat: d3.format(",.1"), tableClass: 'economy', indicatorName: 'Black', accessorFunction: function(d) {return d.bl_or10;}, index: 'No', scored: 'Positive'};
		} else if (indicator == 5) {
			textTick = { text: 'Number', tickFormat: d3.format(",.1"), tableClass: 'economy', indicatorName: 'American Indian and Alaska Native', accessorFunction: function(d) {return d.aian_or10;}, index: 'No', scored: 'Positive'};
		} else if (indicator == 6) {
			textTick = { text: 'Number', tickFormat: d3.format(",.1"), tableClass: 'economy', indicatorName: 'Asian', accessorFunction: function(d) {return d.asia_or10;}, index: 'No', scored: 'Positive'};
		} else if (indicator == 7) {
			textTick = { text: 'Number', tickFormat: d3.format(",.1"), tableClass: 'economy', indicatorName: 'Native Hawaiian and Pacific Islander', accessorFunction: function(d) {return d.nhpi_or10;}, index: 'No', scored: 'Positive' };
		} else if (indicator == 8) {
			textTick = { text: 'Number', tickFormat: d3.format(",.1"), tableClass: 'economy', indicatorName: 'Some Other Race', accessorFunction: function(d) {return d.somoth_or1;}, index: 'No', scored: 'Positive' };
		} else if (indicator == 9) {
			textTick = { text: 'Number', tickFormat: d3.format(",.1"), tableClass: 'economy', indicatorName: 'Total Housing Units', accessorFunction: function(d) {return d.tothu10;}, index: 'No', scored: 'Positive' };
		} else if (indicator == 10) {
			textTick = { text: 'Number', tickFormat: d3.format(",.1"), tableClass: 'economy', indicatorName: 'Total Occupied Units (Households)', accessorFunction: function(d) {return d.totoccu_10;}, index: 'No', scored: 'Positive'  };
		} else if (indicator == 11) {
			textTick = { text: 'Number', tickFormat: d3.format(",.2"), tableClass: 'economy', indicatorName: 'Total Vacant Units', accessorFunction: function(d) {return d.totvach_10;}, index: 'No', scored: 'Positive'  };
		} else if (indicator == 'Faith_imp') {
			textTick = { text: 'Number', tickFormat: d3.format(",.2"), tableClass: 'economy', indicatorName: 'Religious Organizations Impacts', accessorFunction: function(d) {return d.totvach_10;}, index: 'No', scored: 'Positive'  };
		}else if (indicator == 'LUD_SCORE') {
			textTick = { text: 'Number', tickFormat: d3.format(",.2"), tableClass: 'economy', indicatorName: 'Land Use Diversity', accessorFunction: function(d) {return d.totvach_10;}, index: 'No', scored: 'Positive'  };
		}else if (indicator == 'half_Score') {
			textTick = { text: 'Number', tickFormat: d3.format(",.2"), tableClass: 'economy', indicatorName: 'Acess to library', accessorFunction: function(d) {return d.totvach_10;}, index: 'No', scored: 'Positive'  };
		}else if (indicator == 'PA_Score') {
			textTick = { text: 'Number', tickFormat: d3.format(",.2"), tableClass: 'economy', indicatorName: 'Acess to Park', accessorFunction: function(d) {return d.totvach_10;}, index: 'No', scored: 'Positive'  };
		}else if (indicator == 'FF_Dens_Sc') {
			textTick = { text: 'Number', tickFormat: d3.format(",.2"), tableClass: 'economy', indicatorName: 'Fast Food Density', accessorFunction: function(d) {return d.totvach_10;}, index: 'No', scored: 'Positive'  };
		}else if (indicator == 'Food_Score') {
			textTick = { text: 'Number', tickFormat: d3.format(",.2"), tableClass: 'economy', indicatorName: 'Access to Food', accessorFunction: function(d) {return d.totvach_10;}, index: 'No', scored: 'Positive'  };
		}else if (indicator == 'TravelTime') {
			textTick = { text: 'Number', tickFormat: d3.format(",.2"), tableClass: 'economy', indicatorName: 'Mean Travel Time', accessorFunction: function(d) {return d.totvach_10;}, index: 'No', scored: 'Positive'  };
		}else if (indicator == 'Sum_diabet') {
			textTick = { text: 'Number', tickFormat: d3.format(",.2"), tableClass: 'economy', indicatorName: 'Diabetes Location Quotient', accessorFunction: function(d) {return d.totvach_10;}, index: 'No', scored: 'Positive'  };
		}else if (indicator == 'Sum_hhdl') {
			textTick = { text: 'Number', tickFormat: d3.format(",.2"), tableClass: 'economy', indicatorName: 'Hypertensive Heart Disease Location Quotient', accessorFunction: function(d) {return d.totvach_10;}, index: 'No', scored: 'Positive'  };
		}else if (indicator == 'Sum_esoph') {
			textTick = { text: 'Number', tickFormat: d3.format(",.2"), tableClass: 'economy', indicatorName: 'Esophageal Cancer Location Quotient', accessorFunction: function(d) {return d.totvach_10;}, index: 'No', scored: 'Positive'  };
		}else if (indicator == 'Sum_uterin') {
			textTick = { text: 'Number', tickFormat: d3.format(",.2"), tableClass: 'economy', indicatorName: 'Uterine Cancer Location Quotient', accessorFunction: function(d) {return d.totvach_10;}, index: 'No', scored: 'Positive'  };
		}else if (indicator == 'Sum_kidney') {
			textTick = { text: 'Number', tickFormat: d3.format(",.2"), tableClass: 'economy', indicatorName: 'Kidney Cancer Location Quotient', accessorFunction: function(d) {return d.totvach_10;}, index: 'No', scored: 'Positive'  };
		}else if (indicator == 'Sum_ypll75') {
			textTick = { text: 'Number', tickFormat: d3.format(",.2"), tableClass: 'economy', indicatorName: 'Years of Potential Life Lost before age 75 Location Quotient', accessorFunction: function(d) {return d.totvach_10;}, index: 'No', scored: 'Positive'  };
		}else {}
		
		return textTick;
	}
	
	function filterJSON(json, value) {
	  var result = {};
	  result['type'] = json['type']
	  result['crs'] = json['crs']
	  result['features'] = []
	  for (i= 0; i < json.features.length;i++) {
		if (json.features[i].properties.GEOID10_1 === value) {
	
		    result['features'].push(json.features[i]);
		}  
	  }
	
	  return result;
	}


	function position(dot) {
		dot.attr("cx", function(d) { return xScale(x(d)); })
		   .attr("cy", function(d) { return yScale(y(d)); })
		   .attr("r", function(d) { return radiusScale(radius(d)); });
	}

	function order(a, b) {
		return radius(b) - radius(a);
	}

	var isPlural = function(v, exp) {
	  var v = Math.abs(Math.round(v/exp));
	  return v >= 2;
	}	
	
	function calcTotalsByCounty(values, index) {
	  var totals, i, yi, countryData, y, val, max = NaN;

	  for (i=0; i<values.length; i++) {
		countryData = values[i];


	//      if (totals[y] === undefined) totals[y] = NaN;

		  val = str2num(countryData[index]);
		  if (!isNaN(val)) {
	//        if (isNaN(totals[y])) totals[y] = 0;
			if (totals === undefined) totals = 0;
			totals += val;
		  }
		
	  }


	  return totals;
	}

	function str2num(str) {
	  // empty string gives 0 when using + to convert
	  if (str === null || str === undefined || str.length == 0) return NaN;
	  return +str;
	}

	var percentFormat = function(v) {
	  var percent_fmt = d3.format(",.1%");
	  return percent_fmt(v);
	};
	function isNumeric(n) {
	  return !isNaN(parseFloat(n)) && isFinite(n);
	}
	var numberFormat = (function() {
	  var short_fmt = d3.format(",.0f");
	  var nfmt = d3.format(",.1f");
	  var fmt = function(v) {  // remove trailing .0
		var formatted = nfmt(v);
		var m = formatted.match(/^(.*)\.0$/);
		if (m !== null) formatted = m[1];
		return formatted;
	  };
	  return function(v) {
		if (v == null  ||  isNaN(v)) return msg("amount.not-available");
		if (isPlural(v, 1e9)) return msg("amount.billions",  fmt(v / 1e9));
		if (v >= 1e9) return msg("amount.billions.singular",  fmt(v / 1e9));
		if (isPlural(v, 1e6)) return msg("amount.millions",  fmt(v / 1e6));
		if (v >= 1e6) return msg("amount.millions.singular",  fmt(v / 1e6));
	//    if (v >= 1e3) return msg("amount.thousands", fmt(v / 1e3));
		return short_fmt(v);
	  };
	})();

	var spercentFormat = function(v) {
	  var spercent_fmt = d3.format(",.0%");
	  return spercent_fmt(v);
	};
	
	var populationTotals,housingTotals;

	function FormattedData(data,indicatorx,indicatory,indicatorz) {
	populationTotals = calcTotalsByCounty(data,'totpop10');
	housingTotals = calcTotalsByCounty(data,'tothu10');

		return data.map(function(d) {
		  return {
			name: d.name,
			x: d[indicatorx], 
			y: d[indicatory],
			z: d[indicatorz],
			c: d.region,
			populationTotals:populationTotals,
			housingTotals:housingTotals,
			region:d.region,
			totpop10:d.totpop10,
			oner_10:d.oner_10,
			white_or10:d.white_or10,
			bl_or10:d.bl_or10,
			aian_or10:d.aian_or10,
			asia_or10:d.asia_or10,
			nhpi_or10:d.nhpi_or10,
			somoth_or1:d.somoth_or1,
			tothu10:d.tothu10,
			totoccu_10:d.totoccu_10,
			totvach_10:d.totvach_10
		  };
		});
	}
  

		
//bubblechart setup
	function CreateScatterPlotChart(data){
		var minx = d3.min(data, function(d) { return parseFloat(d.x); });
		var miny = d3.min(data, function(d) { return parseFloat(d.y); });
		var minz = d3.min(data, function(d) { return parseFloat(d.z); });
		xScale.domain([0, 1]);
		yScale.domain([0, 1]);
		radiusScale.domain([minz, d3.max(data, function(d) { return parseFloat(d.z); })]);
		var xAxis = d3.svg.axis().orient("bottom").scale(xScale).tickFormat(spercentFormat).tickSize(-SecondRowheight*0.9, 0).tickValues([0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]);
		var yAxis = d3.svg.axis().orient("left").scale(yScale).tickFormat(spercentFormat).tickSize(-SecondRowwidth*0.6, 0).tickValues([0.2, 0.4, 0.6, 0.8]);
		var chartheight = SecondRowheight
		bubblesvg.append("g")
			.attr("class", "x axis")
			.attr("transform", "translate(0," + chartheight + ")")
			.call(xAxis);

		// Add the y-axis.
		bubblesvg.append("g")
			.attr("class", "x axis")
			.call(yAxis);
		
		bubbledot.selectAll(".dot")
			.data(data)
			.enter().append("circle")
			.attr("class", "dot")
			.style("fill", "#C4A37A")
			.style("stroke","white")
			.call(position)
			.on("mouseover", function(d) {
			  d3.select(this).style("fill","#FCB12E");
			  tooltip.html(
						'<h5>' + d.name + '</h5>' +
						'<br>'+
						'<h5>' + spercentFormat(d.x) + '</h5>'+ 
						'<h5>' + $(".bubbleTab #bubbleThirdRow #dd-text").text()+'</h5>'+
						'<h5>' + spercentFormat(d.y) + '</h5>'+ 
						'<h5>' + $(".bubbleTab #bubbleSecondRow #dd-text").text() + '</h5>'+
						'<h5>' + spercentFormat(d.z) + '</h5>'+ 
						'<h5>' + $(".bubbleTab #mapDropDown4 #dd-text").text() + '</h5>'						
						)	
					.style("left", (d3.event.pageX-400) + "px")		
					.style("top", (d3.event.pageY-250 ) + "px")
					.style("opacity", 0.8);	
			})
			.on("mouseout", function() {
			  d3.select(this).style("fill","#C4A37A");
			  tooltip.transition()		
						.duration(500)		
						.style("opacity", 0);
			});

	}

	function UpdateScatterPlotChart(data){
		var minx = d3.min(data, function(d) { return parseFloat(d.x); });
		var miny = d3.min(data, function(d) { return parseFloat(d.y); });
		var minz = d3.min(data, function(d) { return parseFloat(d.z); });
		xScale.domain([0, 1]);
		yScale.domain([0, 1]);
		radiusScale.domain([minz, d3.max(data, function(d) { return parseFloat(d.z); })]);
		var xAxis = d3.svg.axis().orient("bottom").scale(xScale).tickFormat(spercentFormat).tickSize(-SecondRowheight*0.9, 0).tickValues([0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9]);
		var yAxis = d3.svg.axis().orient("left").scale(yScale).tickFormat(spercentFormat).tickSize(-SecondRowwidth*0.6, 0).tickValues([0.2, 0.4, 0.6, 0.8]);
		var chartheight = SecondRowheight
		bubblesvg.select(".x")
			.transition().duration(1500)
			.attr("transform", "translate(0," + chartheight + ")")
			.call(xAxis);
			
		bubblesvg.select(".y")
			.transition().duration(1500)
			.call(yAxis);
		
		
		var circ = bubblesvg.selectAll("circle")	
			.data(data);
			

		circ.transition().duration(1500)
			.attr("class", "dot")
			//.sort(order)
			  
		circ.transition().duration(1500)
	  	    .attr("cx", function(d) { return xScale(x(d)); })
		    .attr("cy", function(d) { return yScale(y(d)); })
		    .attr("r", function(d) { return radiusScale(radius(d)); });
		   
		circ.on("mouseover", function(d) {
			d3.select(this).style("fill","#FCB12E");
			tooltip.html(
						'<h5>' + d.name + '</h5>' +
						'<br>'+
						'<h5>' + spercentFormat(d.x) + '</h5>'+ 
						'<h5>' + $(".bubbleTab #bubbleThirdRow #dd-text").text()+'</h5>'+
						'<h5>' + spercentFormat(d.y) + '</h5>'+ 
						'<h5>' + $(".bubbleTab #bubbleSecondRow #dd-text").text() + '</h5>'+
						'<h5>' + spercentFormat(d.z) + '</h5>'+ 
						'<h5>' + $(".bubbleTab #mapDropDown4 #dd-text").text() + '</h5>'						
						)	
					.style("left", (d3.event.pageX-400) + "px")		
					.style("top", (d3.event.pageY-250 ) + "px")
					.style("opacity", 0.8);				
		   })
		   .on("mouseout", function() {
		   d3.select(this).style("fill","#C4A37A");	
		   tooltip.transition()		
						.duration(500)		
						.style("opacity", 0);
		   });
	}
	
// load data 
	d3.json("data/county_plotchart.json", function(json) {
		var indicatorx = textToID($(".bubbleTab #bubbleThirdRow #dd-text").text());
		var indicatory = textToID($(".bubbleTab #bubbleSecondRow #dd-text").text());
		var indicatorz = textToID($(".bubbleTab #mapDropDown4 #dd-text").text());
		var DataForm = FormattedData(json,indicatorx,indicatory,indicatorz);
		var FiltedDataFormByIndex = UpdateDataByIndexDropDownMenu(DataForm,indicatorx,indicatory,indicatorz);
		CreateScatterPlotChart(FiltedDataFormByIndex); 
		//UpdateScatterPlotChartbyIndexDropDownMenu(DataForm);
		
		$(".bubbleTab #mapDropDown4 #menu45 li a").click(function(){
			$(".bubbleTab #mapDropDown4 #dd-text").text($(this).text());
			var fieldVal = textToID($(this).text()); 
			var indicatorx = textToID($(".bubbleTab #bubbleThirdRow #dd-text").text());
			var indicatory = textToID($(".bubbleTab #bubbleSecondRow #dd-text").text());
			var indicatorz = textToID($(".bubbleTab #mapDropDown4 #dd-text").text());
			var DataForm = FormattedData(json,indicatorx,indicatory,indicatorz);
			var FiltedDataFormByIndex = UpdateDataByIndexDropDownMenu(DataForm,indicatorx,indicatory,indicatorz);
			UpdateScatterPlotChart(FiltedDataFormByIndex); 
		})
		
		$(".bubbleTab #bubbleSecondRow #menu45 li a").click(function(){
			$(".bubbleTab #bubbleSecondRow #dd-text").text($(this).text());
			var fieldVal = textToID($(this).text()); 
			var indicatorx = textToID($(".bubbleTab #bubbleThirdRow #dd-text").text());
			var indicatory = textToID($(".bubbleTab #bubbleSecondRow #dd-text").text());
			var indicatorz = textToID($(".bubbleTab #mapDropDown4 #dd-text").text());
			var DataForm = FormattedData(json,indicatorx,indicatory,indicatorz);
			var FiltedDataFormByIndex = UpdateDataByIndexDropDownMenu(DataForm,indicatorx,indicatory,indicatorz);
			UpdateScatterPlotChart(FiltedDataFormByIndex); 
		})

		$(".bubbleTab #bubbleThirdRow #menu45 li a").click(function(){
			$(".bubbleTab #bubbleThirdRow #dd-text").text($(this).text());
			var fieldVal = textToID($(this).text()); 
			var indicatorx = textToID($(".bubbleTab #bubbleThirdRow #dd-text").text());
			var indicatory = textToID($(".bubbleTab #bubbleSecondRow #dd-text").text());
			var indicatorz = textToID($(".bubbleTab #mapDropDown4 #dd-text").text());
			var DataForm = FormattedData(json,indicatorx,indicatory,indicatorz);
			var FiltedDataFormByIndex = UpdateDataByIndexDropDownMenu(DataForm,indicatorx,indicatory,indicatorz);
			UpdateScatterPlotChart(FiltedDataFormByIndex); 
		})		
	});
});
