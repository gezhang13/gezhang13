$(document).ready(function() {
	var firstButtonGrp = $('.btn-collection-1 .btn');

	firstButtonGrp.click(function () {
		var prevImg = $('.btn-primary').find('img');

		if(prevImg.attr('src') === 'img/icons/MapSelected.png') { 
		    prevImg.attr('src','img/icons/Map.png');
		}
		else if(prevImg.attr('src') === 'img/icons/BarChartSelected.png') {
			prevImg.attr('src','img/icons/BarChart.png');
		}
		else if(prevImg.attr('src') === 'img/icons/BubbleChartSelected.png') {
			prevImg.attr('src','img/icons/BubbleChart.png');
		}
		else if(prevImg.attr('src') === 'img/icons/SpiderSelected.png') {
			prevImg.attr('src','img/icons/Spider.png');
		}

	    firstButtonGrp.removeClass("btn-primary").addClass("btn-default");
	    $(this).removeClass("btn-default").addClass("btn-primary");

	    var activeImg = $(this).find('img');

	    if(activeImg.attr('src') === 'img/icons/Map.png') { 
		    activeImg.attr('src','img/icons/MapSelected.png');
		}
		else if(activeImg.attr('src') === 'img/icons/BarChart.png') {
			activeImg.attr('src','img/icons/BarChartSelected.png');
		}
		else if(activeImg.attr('src') === 'img/icons/BubbleChart.png') {
			activeImg.attr('src','img/icons/BubbleChartSelected.png');
		}
		else if(activeImg.attr('src') === 'img/icons/Spider.png') {
			activeImg.attr('src','img/icons/SpiderSelected.png');
		}
	});
});