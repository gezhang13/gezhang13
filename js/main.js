jQuery(document).ready(function(){
	//cache DOM elements
	var mainContent = $('.cd-main-content'),
		header = $('.cd-main-header'),
		sidebar = $('.cd-side-nav'),
		sidebarTrigger = $('.cd-nav-trigger'),
		topNavigation = $('.cd-top-nav'),
		titleHeading = $('.cd-title-heading'),
		aboutTab = $('.cd-about-tab'),
		dataTab = $('.cd-data-tab');

	//on resize, move search and top nav position according to window width
	var resizing = false;
	moveNavigation();
	$(window).on('resize', function(){
		if( !resizing ) {
			(!window.requestAnimationFrame) ? setTimeout(moveNavigation, 300) : window.requestAnimationFrame(moveNavigation);
			resizing = true;
		}
	});

	//on window scrolling - fix sidebar nav
	var scrolling = false;
	checkScrollbarPosition();
	$(window).on('scroll', function(){
		if( !scrolling ) {
			(!window.requestAnimationFrame) ? setTimeout(checkScrollbarPosition, 300) : window.requestAnimationFrame(checkScrollbarPosition);
			scrolling = true;
		}
	});

	//mobile only - open sidebar when user clicks the hamburger menu
	/*sidebarTrigger.on('click', function(event){
		event.preventDefault();
		$([sidebar, sidebarTrigger]).toggleClass('nav-is-visible');
	});*/

	//click on item and show submenu
	$('.has-children > a').on('click', function(event){
		var mq = checkMQ(),
			selectedItem = $(this);
		if( mq == 'mobile' || mq == 'tablet' ) {
			event.preventDefault();
			if( selectedItem.parent('li').hasClass('selected')) {
				selectedItem.parent('li').removeClass('selected');
			} else {
				sidebar.find('.has-children.selected').removeClass('selected');
				selectedItem.parent('li').addClass('selected');
			}
		}
	});

	sidebar.find('li').click(function(e) {
		changeViewOnClick($(this));
    });

	sidebar.find('li').hover(function(e){
		hoverIn($(this));
	}, function(e) {
		hoverOut($(this));
	} );

	$(document).on('click', function(event){
		if( !$(event.target).is('.has-children a') ) {
			sidebar.find('.has-children.selected').removeClass('selected');
		}
	});

	//on desktop - differentiate between a user trying to hover over a dropdown item vs trying to navigate into a submenu's contents
	sidebar.children('ul').menuAim({
        activate: function(row) {
        	$(row).addClass('hover');
        },
        deactivate: function(row) {
        	$(row).removeClass('hover');
        },
        exitMenu: function() {
        	sidebar.find('.hover').removeClass('hover');
        	return true;
        },
        submenuSelector: ".has-children",
    });

	// Functions Start from here
	function hoverIn(thisObj) {
		var hoverImage = thisObj.find('img');
		if (!thisObj.hasClass('active')) {
			if(hoverImage.attr('src') === 'img/icons/aboutProject.png') { 
		    	hoverImage.attr('src','img/icons/aboutProjectSelected.png');
		    }
		    else if(hoverImage.attr('src') === 'img/icons/Dashboard.png') {
		    	hoverImage.attr('src','img/icons/DashboardSelected.png');
		    }
		    else if(hoverImage.attr('src') === 'img/icons/Resources.png') {
		    	hoverImage.attr('src','img/icons/ResourcesSelected.png');
		    }
	    	else if(hoverImage.attr('src') === 'img/icons/ContactUs.png') {
	    		hoverImage.attr('src','img/icons/ContactUsSelected.png');
	    	}
    	}
	}

	function hoverOut(thisObj) {
		var hoverImage = thisObj.find('img');
		if(!thisObj.hasClass('active')) {
			if(hoverImage.attr('src') === 'img/icons/aboutProjectSelected.png') { 
		    	hoverImage.attr('src','img/icons/aboutProject.png');
		    }
		    else if(hoverImage.attr('src') === 'img/icons/DashboardSelected.png') {
		    	hoverImage.attr('src','img/icons/Dashboard.png');
		    }
		    else if(hoverImage.attr('src') === 'img/icons/ResourcesSelected.png') {
		    	hoverImage.attr('src','img/icons/Resources.png');
		    }
	    	else if(hoverImage.attr('src') === 'img/icons/ContactUsSelected.png') {
	    		hoverImage.attr('src','img/icons/ContactUs.png');
	    	}
    	}
	}

	function changeViewOnClick(thisObj) {
		var prevImage = sidebar.find('li.active').find('img');
		if(sidebar.find('li.active')[0] != thisObj[0]) {
			if(prevImage.attr('src') === 'img/icons/aboutProjectSelected.png') { 
            	prevImage.attr('src','img/icons/aboutProject.png');
            	aboutTab.hide();
        	}
        	else if(prevImage.attr('src') === 'img/icons/DashboardSelected.png') {
        		prevImage.attr('src','img/icons/Dashboard.png');
        		dataTab.hide();
        	}
        	else if(prevImage.attr('src') === 'img/icons/ResourcesSelected.png') {
        		prevImage.attr('src','img/icons/Resources.png');
        	}
        	else if(prevImage.attr('src') === 'img/icons/ContactUsSelected.png') {
        		prevImage.attr('src','img/icons/ContactUs.png');
        	}
		}
		sidebar.find('li').removeClass('active');
        if (!thisObj.hasClass('active')) {
            thisObj.addClass('active');
            objImage = thisObj.find('img');
            if(objImage.attr('src') === 'img/icons/aboutProjectSelected.png') { 
		    	aboutTab.show();
		    }
		    else if(objImage.attr('src') === 'img/icons/DashboardSelected.png') {
		    	dataTab.show();
		    }
		    else if(objImage.attr('src') === 'img/icons/ResourcesSelected.png') {
		    
		    }
	    	else if(objImage.attr('src') === 'img/icons/ContactUsSelected.png') {
	    		
	    	}
        }
	}

	function checkMQ() {
		//check if mobile or desktop device
		return window.getComputedStyle(document.querySelector('.cd-main-content'), '::before').getPropertyValue('content').replace(/'/g, "").replace(/"/g, "");
	}

	function moveNavigation(){
  		var mq = checkMQ();
        
        if ( mq == 'mobile' && topNavigation.parents('.cd-side-nav').length == 0 ) {
        	detachElements();
			topNavigation.appendTo(sidebar);
			/*titleHeading.removeClass('is-hidden').prependTo(sidebar);*/
		} else if ( ( mq == 'tablet' || mq == 'desktop') &&  topNavigation.parents('.cd-side-nav').length > 0 ) {
			detachElements();
			topNavigation.appendTo(header.find('.cd-nav'));
		}
		checkSelected(mq);
		resizing = false;
	}

	function detachElements() {
		topNavigation.detach();
		titleHeading.detach();
	}

	function checkSelected(mq) {
		//on desktop, remove selected class from items selected on mobile/tablet version
		if( mq == 'desktop' ) $('.has-children.selected').removeClass('selected');
	}

	function checkScrollbarPosition() {
		var mq = checkMQ();
		
		if( mq != 'mobile' ) {
			var sidebarHeight = sidebar.outerHeight(),
				windowHeight = $(window).height(),
				mainContentHeight = mainContent.outerHeight(),
				scrollTop = $(window).scrollTop();

			( ( scrollTop + windowHeight > sidebarHeight ) && ( mainContentHeight - sidebarHeight != 0 ) ) ? sidebar.addClass('is-fixed').css('bottom', 0) : sidebar.removeClass('is-fixed').attr('style', '');
		}
		scrolling = false;
	}
});