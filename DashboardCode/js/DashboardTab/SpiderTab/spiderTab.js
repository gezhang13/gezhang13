$(document).ready(function() { 
	//var topicarray = ["Percent of Fulton Population"]
	//console.log(getTextWidth('xxmanxxxxxxxxxx','19px Roboto'))
	//Set up the menu 

	


	//Functions
	var spercentFormat = function(v) {
	  var spercent_fmt = d3.format(",.1%");
	  return spercent_fmt(v);
	};
	
	function stringParser(strArr) {
		var res = "";
		for (i = 2; i < strArr.length; i++) {
			var temp = strArr[i];
			if (i != strArr.length-1)
				temp = temp.concat(" "); 
    		res = res.concat(temp);
		}
		return res;
	}
	
	function truncateString(strArr) {
		var res = "";
		for (i=0; i<strArr.length-1; i++) {
			var temp = strArr[i];
			if (i < strArr.length-2)
				temp = temp.concat(" ");
			res = res.concat(temp);
		}
		return res;
	}
	
	function getLabel(indicator) {
		var textTick = {};
		return textTick;
	}
	
	function textToID(strVal) {
		var res = "";
		if (strVal == "Percent of Fulton Population") {
			res = "totpop10";
		}
		else if(strVal == "Percent of One Race Total") {
			res = "oner_10";
		}
		else if(strVal == "Percent of White") {
			res = "white_or10";
		}
		else if(strVal == "Percent of Black") {
			res = "bl_or10";
		}
		else if(strVal == "Percent of American Indian") {
			res = "aian_or10";
		}
		else if(strVal == "Percent of Asian") {
			res = "asia_or10";
		}
		else if(strVal == "Percent of Some Other Race") {
			res = "somoth_or1";
		}
		else if(strVal == "Percent of Fulton Housing Units") {
			res = "tothu10";
		}
		else if(strVal == "Percent of Total Occupied Units") {
			res = "totoccu_10";
		}
		else if(strVal == "Percent of Total Vacant Units") {
			res = "totvach_10";
		}
		return res;
	}
	
	  	
		
// scatterplot chart
    var spiderwebheight = $('.well').height()*0.9;
	var spiderwebwidth = $('.well').width();

	d3.select("#tab4")
	  .append("div")
	  .attr("id","spiderwebchart")
	  .style({'width':spiderwebwidth+'px','height':spiderwebheight+'px', 'margin-left':'15px',"margin-top":'0px'});
	  
			  
	function calcTotalsByCounty(values, index) {
		var totals, i, yi, countryData, y, val, max = NaN;

		for (i=0; i<values.length; i++) {
			countryData = values[i];
			val = str2num(countryData[index]);
			if (!isNaN(val)) {
				if (totals === undefined) totals = 0;
				totals += val;
			}
		}
		return totals;
	}
	
	function str2num(str) {
	  // empty string gives 0 when using + to convert
	  if (str === null || str === undefined || str.length == 0) return NaN;
	  return +str;
	}
	
	function pickIndicatorData(dataset, indicator) {
			var populationTotals = calcTotalsByCounty(dataset,'totpop10');
			var housingTotals = calcTotalsByCounty(dataset,'tothu10');
				//console.log(indicator)
			var fieldID = textToID($(".spiderTab #dd-text").text());
			if (fieldID=='totpop10') {	
			$.each(dataset, function(i,d) {
				d.axis = d['name']
				d.value = parseInt(d[indicator])/parseInt(populationTotals);	
				d.totpop10 = d['totpop10'];
				d.housing = d['tothu10']
				d.populationTotals = populationTotals;
				d.housingTotals = housingTotals;
			});
			}
			else if (fieldID=='oner_10'|| fieldID=='white_or10'||
			fieldID=='bl_or10'||fieldID=='aian_or10'||
			fieldID=='asia_or10'||fieldID=='somoth_or1') {
				$.each(dataset, function(i,d) {
				d.axis = d['name']
				d.value = parseInt(d[indicator])/parseInt(d['totpop10']);	
				d.totpop10 = d['totpop10'];
				d.housing = d['tothu10']
				d.populationTotals = populationTotals;
				d.housingTotals = housingTotals;
			});
			}
			else if (fieldID=='tothu10') {
				$.each(dataset, function(i,d) {
				d.axis = d['name']
				d.value = parseInt(d[indicator])/parseInt(housingTotals);	
				d.totpop10 = d['totpop10'];
				d.housing = d['tothu10']
				d.populationTotals = populationTotals;
				d.housingTotals = housingTotals;
			});
			}
			else if (fieldID=='totoccu_10'||fieldID=='totvach_10') {
			$.each(dataset, function(i,d) {
				d.axis = d['name']
				d.value = parseInt(d[indicator])/parseInt(d['tothu10']);	
				d.totpop10 = d['totpop10'];
				d.housing = d['tothu10']
				d.populationTotals = populationTotals;
				d.housingTotals = housingTotals;
			});	
			}
			else {
			$.each(dataset, function(i,d) {
				d.axis = d['name']
				d.value = d[indicator];	
				d.totpop10 = d['totpop10'];
				d.housing = d['tothu10']
				d.populationTotals = populationTotals;
				d.housingTotals = housingTotals;
			});
			}
			return dataset;
	}
	
	d3.json("data/county_plotchart.json", function(data) { 	
			var origindata = data;
			var datasetIndicators = origindata;
			// pick initial indicator
			var indicator = textToID($(".spiderTab #dd-text").text());
			// pick initial order, set indicator to d.value and order for plotting
			var filteredDataByIndicator = pickIndicatorData(datasetIndicators,indicator);
			// get text and ticks
			var textTick = getLabel(indicator);
			var spiderdataset=[filteredDataByIndicator];
			
			 var mycfg = {
			  w: spiderwebheight,
			  h: spiderwebheight,
			  maxValue: 0.6,
			  levels: 6,
			  ExtraWidthX: 300
			}
			
			RadarChart.draw("#spiderwebchart", spiderdataset, mycfg);
				
			$(".spiderTab #menu5 li a").click(function(){
				var strArray = $(this).text().split(" ");
				var parsedString = stringParser(strArray);

				$(".spiderTab #dd-text").text($(this).text());
				var fieldVal = textToID($(this).text()); 
				//topicarray=[($(this).text())];	
				
				var indicator = textToID($(".spiderTab #dd-text").text());
				// pick initial order, set indicator to d.value and order for plotting
				var filteredDataByIndicator = pickIndicatorData(datasetIndicators,indicator);
				// get text and ticks
				var textTick = getLabel(indicator);
				var spiderdataset=[filteredDataByIndicator];
				
				 var mycfg = {
				  w: spiderwebheight,
				  h: spiderwebheight,
				  maxValue: 0.3,
				  levels: 6,
				  ExtraWidthX: 300
				}
				
				RadarChart.draw("#spiderwebchart", spiderdataset, mycfg);
			
			});
		});
		
	 
});
