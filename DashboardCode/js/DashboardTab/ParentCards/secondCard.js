$(document).ready(function() {
	var secondButtonGrp = $('.btn-collection-2 .btn');
	secondButtonGrp.click(function () {
		secondButtonGrp.removeClass("btn-primary").addClass("btn-default");
	    $(this).removeClass("btn-default").addClass("btn-primary");
	});
});