$(document).ready(function() {
	$(".mapTab #menu1 li a").click(function(){
		var strArray = $(this).text().split(" ");
		var parsedString = stringParser(strArray);
		$(".mapTab #dropDownText").text(parsedString);
		$(".mapTab #dd-text").text($(this).text());
		var fieldVal = textToID($(this).text());   
		updateBubbleSizes(fieldVal);
		updateToolTip();
   	});

   	$("#menu2 li a").click(function(){
		$(".mapTab #dd-text2").text($(this).text());
		if (features != null) {
			updateColorForFirstMap(features);
			updateColorLegend(features);
			updateToolTip();
		}
   	});

	function textToID(strVal) {
		var res = "";
		if (strVal == "Percent of Fulton Population") {
			res = "totpop10";
		}
		else if(strVal == "Percent of One Race Total") {
			res = "oner_10";
		}
		else if(strVal == "Percent of White") {
			res = "white_or10";
		}
		else if(strVal == "Percent of Black") {
			res = "bl_or10";
		}
		else if(strVal == "Percent of American Indian") {
			res = "aian_or10";
		}
		else if(strVal == "Percent of Asian") {
			res = "asia_or10";
		}
		else if(strVal == "Percent of Some Other Race") {
			res = "somoth_or1";
		}
		else if(strVal == "Percent of Fulton Housing Units") {
			res = "tothu10";
		}
		else if(strVal == "Percent of Total Occupied Units") {
			res = "totoccu_10";
		}
		else if(strVal == "Percent of Total Vacant Units") {
			res = "totvach_10";
		}

		return res;
	}

	// Imported Code Starts from here

	var heightOffset = 70;
	var landColor = d3.rgb("#666666");  //1e2b32 .brighter(2)
	var width = height = width_sec = null;
	var w,h;
	var chart_svg = d3.select("#chart").append("svg");
	var chart_sec_svg = d3.select("#chart_Sec").append("svg");
	var background = chart_svg.append("rect");
	var background_sec = chart_sec_svg.append("rect");
	var firstMapColors = d3.scale.linear()
		.range(["#F4DFD9","#D06F52"]) //#F4DFD9: lighter shade
		.interpolate(d3.interpolateHcl);
	migrationsColor = d3.scale.log()
	    .range(["#E0E0E0","#212121"])
	    .interpolate(d3.interpolateHcl);
			
	var rscale = d3.scale.sqrt();

	function initSizes() {
		width = $("#mapCol").width();
		height = $("#mapThirdRow").height();
		width_sec = $("#mapCol_Sec").width();
		height_sec = $("#mapCol_Sec").height(); 
		w = width;
		h = height;
		background.attr("width", width).attr("height", height);
		background_sec.attr("width", width_sec).attr("height", height_sec);
		chart_svg.attr("width", width).attr("height", height);
		chart_sec_svg.attr("width", width_sec).attr("height", height_sec);
		rscale.range([0, height/45]);
	};

	initSizes();

	var isPlural = function(v, exp) {
		var v = Math.abs(Math.round(v/exp));
		return v >= 2;
	}

	var numberFormat = (function() {
		var short_fmt = d3.format(",.0f");
		var nfmt = d3.format(",.1f");
		var fmt = function(v) {  // remove trailing .0
			var formatted = nfmt(v);
			var m = formatted.match(/^(.*)\.0$/);
			if (m !== null) formatted = m[1];
				return formatted;
		};
		return function(v) {
		if (v == null  ||  isNaN(v)) return msg("amount.not-available");
		if (isPlural(v, 1e9)) return msg("amount.billions",  fmt(v / 1e9));
		if (v >= 1e9) return msg("amount.billions.singular",  fmt(v / 1e9));
		if (isPlural(v, 1e6)) return msg("amount.millions",  fmt(v / 1e6));
		if (v >= 1e6) return msg("amount.millions.singular",  fmt(v / 1e6));
			return short_fmt(v);
		};
	})();

	var moneyFormat = function(v) {
		if (v == null  ||  isNaN(v)) 
			return msg("amount.not-available");
	  	return msg("money", numberFormat(v));
	};

	var moneyMillionsFormat = function(v) { 
		return moneyFormat(1e6 * v);
	};

	function str2num(str) {
		// empty string gives 0 when using + to convert
		if (str === null || str === undefined || str.length == 0) return NaN;
		return +str;
	}

	var percentFormat = function(v) {
		var percent_fmt = d3.format(",.1%");
		return percent_fmt(v);
	};

	var spercentFormat = function(v) {
		var spercent_fmt = d3.format(",.1%");
		return spercent_fmt(v);
	};
			  
	var selectedYear = null;
	var selectedCountry = null, highlightedCountry = null, secondHighlightedCountry = null;
	countryNamesByCode = {};
	countryCodeByName = {};

	$("#color-legend").fadeIn();

	/* @param values is an array in which the indices correspond to the indices in the remittenceYears array */
	function calcTotalsByCounty(values, index) {
		var totals, i, yi, countryData, y, val, max = NaN;
		for (i=0; i<values.length; i++) {
			countryData = values[i];
		 	val = str2num(countryData[index]);
		  	if (!isNaN(val)) {
		    	if (totals === undefined) totals = 0;
		    	totals += val;
		  	}
		}
		return totals;
	}

	function calcTotalsByCity(values, index) {
		var totals={}, i, yi, countryData, y, val, max = NaN;

		for (i=0; i<values.length; i++) {
			countryData = values[i];
			val = str2num(countryData[index]);
		  	if (!isNaN(val)) {
		    	if (totals === undefined) totals = 0;
		    	totals[countryData.GEOID10] = val;
		  	}
		}
		return totals;
	}
	
	function updateDetails() {
		var countryName, totalPopulation, numMigrants;

		if (secondHighlightedCountry != null  ||  secondSelectedCountry != null) {
			var iso3 = (secondSelectedCountry || secondHighlightedCountry);
			countryName = countryNamesByCode[iso3];

			var countryRem = totalPopArr[iso3];
			totalPopulation = (countryRem != null ? str2num(countryRem) : NaN);
			numMigrants = housingTotalsbycity[iso3];
			totalAid = occuTotalsbycity[iso3];
			totalvacant = vacantTotalsbycity[iso3];
		} 
		else {
			totalPopulation = totalPop;
			numMigrants = housingTotals;
			totalvacant = vacantTotals;
			totalAid = occuTotals;
		}

		if (secondHighlightedCountry)
			$("#cityText_Sec").text(countryName);
		else if(secondSelectedCountry == null)
			$("#cityText_Sec").text("Fulton County");
		
		$("#text_1_1_2_1").text(numberFormat(totalPopulation)); // population
		$("#text_1_2_2_1").text(numberFormat(totalAid)); // occupied
		$("#text_2_1_2_1").text(numberFormat(numMigrants)); // housing units
		$("#text_2_2_2_1").text(numberFormat(totalvacant)); // vacant housing units
	}

	function selectYear(year, duration) {
		selectedYear = year;
		updateBubbleSizes('totpop10');
		updateDetails();
	}

	function updateBubbleSizes(strVal) {
		d3.selectAll("#chart g.countries circle").attr("r", function(d) {
			var bubbleindex = strVal;
			var v = d[bubbleindex], r;

			if (strVal=='totpop10'){
		  		v = v/totalPop;
		  	}
		  	else if (strVal=='oner_10'|| strVal=='white_or10'|| strVal=='bl_or10'|| strVal=='aian_or10'|| strVal=='asia_or10'|| strVal=='somoth_or1'){
				v = v/d['totpop10'];  
		  	}
		  	else if (strVal=='tothu10'){
				v = v/housingTotals;  
		  	}
		  	else if (strVal=='totoccu_10'||strVal=='totvach_10'){
				v = v/d['tothu10'];    
		  	}
		  	else{
				v = v;
		  	}
			  
			r = rscale(v)*700*1.5;
			return (isNaN(r) ? 0 : r);
		});
	}

	$(document).keyup(function(e) { if (e.keyCode == 27) selectCountry(null); });

	function highlightCountry(code, data, isFirstMap) {
		if (isFirstMap)
			highlightedCountry = code;
		else
			secondHighlightedCountry = code;
		if (isFirstMap) {
			chart_svg.selectAll("path.land")
			updateColorForFirstMap(data);
		}
		else {
			updateColorForSecondMap(data);
		}
	}

	function getTotalVal(fieldID, data) {
		if (fieldID=='totpop10') {
			return totalPop;
		}
		else if (fieldID=='oner_10'|| fieldID=='white_or10'|| fieldID=='bl_or10'|| 
			fieldID=='aian_or10'|| fieldID=='asia_or10'|| fieldID=='somoth_or1') {
			return data.properties['totpop10'];
		}
		else if (fieldID=='tothu10') {
			return housingTotals;
		}
		else if (fieldID=='totoccu_10'||fieldID=='totvach_10') {
			return data.properties['tothu10'];   
		}
		else { 
			return 1;
		}
	}

	function updateColorForFirstMap(data) {
		var totalVal;

		var gcountries = chart_svg.select("g.countries");
		var fieldID = textToID($(".mapTab #dd-text2").text());

		var max = d3.max(data, function(d) { return d.properties[fieldID]; }); // calc max over time for country
		var val;
		var min = d3.min(data, function(d) { return d.properties[fieldID]; }); // calc max over time for country

		mink = d3.min(data, function(d) { 
			// Get Total Value for Getting right color code
			totalVal = getTotalVal(fieldID,d);
			return d.properties[fieldID]/totalVal;
		});

	    maxk = d3.max(data, function(d) {
			// Get Total Value for Getting right color code
	    	totalVal = getTotalVal(fieldID,d);
			return d.properties[fieldID]/totalVal;
		});

		chart_svg.selectAll("path.land")
		   .classed("highlighted", false)
		   .classed("selected", false)
		   .transition()
		   .duration(50)
		   .attr("fill",function(d) {
	   			val = d.properties[fieldID];
				totalVal = getTotalVal(fieldID,d);

				if(maxk < 1) {
					d3.select("#color-legend").select("g.color-legend").select(".axisx").text(percentFormat(mink))
					d3.select("#color-legend").select("g.color-legend").select(".axisy").text(percentFormat(maxk))
				}
				else {
					d3.select("#color-legend").select("g.color-legend").select(".axisx").text(numberFormat(mink))
					d3.select("#color-legend").select("g.color-legend").select(".axisy").text(numberFormat(maxk))
				}

				firstMapColors.domain([mink, maxk]); 
				val = val/totalVal;
		    	return firstMapColors(val);
			})
			.attr("stroke", "white");

				
		if (selectedCountry === null  &&  highlightedCountry == null) {			
			gcountries.selectAll("circle.country")
			.attr("opacity", 0.6)
			.attr("fill", "#5A5A5A");
		} 
		else 
		{
			gcountries.selectAll("circle.country").attr("opacity", function(d) {
		        if (d.GEOID10 === selectedCountry || (selectedCountry == null && d.GEOID10 == highlightedCountry))
		        	return 0.6;
		        else
		        	return 0;
		    })
		    .attr("fill", function(d) {
		    	if (d.GEOID10 === selectedCountry || (selectedCountry == null && d.GEOID10 == highlightedCountry))
		        	return "#FCB12E";
		        else
		        	return "#C4A37A";
		    });
		}	

	}

	function updateColorForSecondMap(data) {
		var fieldID = "totpop10";
		var val;
		mink = d3.min(data, function(d) { 
				return d.properties[fieldID]/totalPop;
			});
	    maxk = d3.max(data, function(d) {
			return d.properties[fieldID]/totalPop;
		});
		chart_sec_svg.selectAll("path.land")
		   .classed("highlighted", false)
		   .classed("selected", false)
		   .transition()
		   .duration(50)
		   .attr("fill",function(d) {
		   		if (secondSelectedCountry && d.properties.GEOID10 === secondSelectedCountry ||
		   			secondSelectedCountry == null && d.properties.GEOID10 == secondHighlightedCountry)
		   			return "#FCB12E";
		   		else {
		   			val = d.properties[fieldID];
					if(maxk < 1) {
						d3.select("#color-legend").select("g.color-legend").select(".axisx").text(percentFormat(mink))
						d3.select("#color-legend").select("g.color-legend").select(".axisy").text(percentFormat(maxk))
					}
					else {
						d3.select("#color-legend").select("g.color-legend").select(".axisx").text(numberFormat(mink))
						d3.select("#color-legend").select("g.color-legend").select(".axisy").text(numberFormat(maxk))
					}

					migrationsColor.domain([mink+0.1, maxk+0.1]); 
					val = val/totalPop+0.1;
			    	return migrationsColor(val);
		   		}
			})
			.attr("stroke", "white");
	}

	function selectCountry(code, data, dontUnselect, isFirstMap) {
		if (isFirstMap) {
			if (selectedCountry === code) {
				if (dontUnselect)
					return;
				selectedCountry = null;
			} 
			else {
				selectedCountry = code;
			}
		}
		else {
			if (secondSelectedCountry === code) {
				if (dontUnselect)
					return;
				secondSelectedCountry = null;
			} 
			else {
				secondSelectedCountry = code;
			}
		}
		
		if (isFirstMap)
			updateColorForFirstMap(data);
		else
			updateSecondMap(data);
	}

	function updateSecondMap(data) {
		var gcountries = chart_sec_svg.select("g.countries");

		var fieldID = textToID($(".mapTab #dd-text2").text());
		var max = d3.max(data, function(d) { return d.properties[fieldID]; }); // calc max over time for country
		var val;
		var min = d3.min(data, function(d) { return d.properties[fieldID]; }); // calc max over time for country
	
		mink = d3.min(data, function(d) { 
				return d.properties[fieldID]/totalPop
			});
	    maxk = d3.max(data, function(d) {
			return d.properties[fieldID]/totalPop
		});
			  
		chart_sec_svg.selectAll("path.land")
		   .classed("highlighted", false)
		   .classed("selected", false)
		   .transition()
		   .duration(50)
		   .attr("fill",function(d) {
		   		if (secondSelectedCountry && d.properties.GEOID10 === secondSelectedCountry)
		   			return "#FCB12E";
		   		else {
		   			val = d.properties[fieldID];
					if(maxk < 1) {
						d3.select("#color-legend").select("g.color-legend").select(".axisx").text(percentFormat(mink))
						d3.select("#color-legend").select("g.color-legend").select(".axisy").text(percentFormat(maxk))
					}
					else {
						d3.select("#color-legend").select("g.color-legend").select(".axisx").text(numberFormat(mink))
						d3.select("#color-legend").select("g.color-legend").select(".axisy").text(numberFormat(maxk))
					}

					migrationsColor.domain([mink+0.1, maxk+0.1]); 
					val = val/totalPop+0.1;
			    	return migrationsColor(val);
		   		}
			})
			.attr("stroke", "white");
	}

	function selectBar(code) {
		if (selectedBar == code) {
			selectedBar = null;
		} 
		else {
			selectedBar = code;
		}

		updateBar(selectedBar);
	}

	function updateBar(code) {
		if (datasetIndicators != null) {
			var svg = d3.select("#barChart svg .mainWrapper");
			var fieldID =  textToID($(".barTab #dd-text").text());
			svg.selectAll("rect")
			.style("fill", function(d) {
				if (code != null || selectedBar != null) {
					var correctCode = code || selectedBar;
					var cityName = countryNamesByCode[correctCode];

					if (cityName.toUpperCase() == d.name.toUpperCase()) {
						return "#FCB12E";
					}
					else {
						if (fieldID=='totpop10') {
							currentvalue = d.value/d.populationTotals;
						}
						else if (fieldID=='oner_10'|| fieldID=='white_or10'||
							fieldID=='bl_or10'||fieldID=='aian_or10'||
							fieldID=='asia_or10'||fieldID=='somoth_or1') {
							currentvalue = d.value/d.totpop10;
						}
						else if (fieldID=='tothu10') {	
							currentvalue = d.value/d.housingTotals;
						}
						else if (fieldID=='totoccu_10'||fieldID=='totvach_10') {
							currentvalue = d.value/d.housing;
						}
						else {  
							currentvalue = d.value;
						}

						return colorScale(currentvalue);
					}
				}
				else {
					if (fieldID=='totpop10') {
						currentvalue = d.value/d.populationTotals;
					}
					else if (fieldID=='oner_10'|| fieldID=='white_or10'||
						fieldID=='bl_or10'||fieldID=='aian_or10'||
						fieldID=='asia_or10'||fieldID=='somoth_or1') {
						currentvalue = d.value/d.totpop10;
					}
					else if (fieldID=='tothu10') {	
						currentvalue = d.value/d.housingTotals;
					}
					else if (fieldID=='totoccu_10'||fieldID=='totvach_10') {
						currentvalue = d.value/d.housing;
					}
					else {  
						currentvalue = d.value;
					}

					return colorScale(currentvalue);
				}
			})
		}
	}

	function initCountriesTypeahead(data) {
		var countryNames = d3.keys(countryNamesByCode).map(function(GEOID10) {
		    return { GEOID10:GEOID10, name:countryNamesByCode[GEOID10] };
		});
		var typeaheadSelect = function(event, d) {
			selectCountry(d.GEOID10, data, true);
		};
	}

	function initCountryNames(remittances) {
		remittances.forEach(function(r) {
			r.centroid = [+r.lon, +r.lat];
			countryNamesByCode[r.GEOID10] = r.NAMELSAD10;
			var nameKey = r.NAMELSAD10.toUpperCase();
			countryCodeByName[nameKey] = r.GEOID10;
		});
	}

	function updateToolTip() {
		var num_val, val, second_val, second_num_val;
    	var fieldID = textToID($(".mapTab #dd-text").text());
    	var second_fieldID = textToID($(".mapTab #dd-text2").text());
    	var fieldText = $(".mapTab #dd-text").text();
    	var second_fieldText = $(".mapTab #dd-text2").text();
    	
    	// Init global iso3 with Atlanta Iso
    	if (global_iso3 == null)
    		global_iso3 = "1304000";

	 	val = (tempData.properties[fieldID]);
	 	second_val = (tempData.properties[second_fieldID]);

		if (fieldID=='totpop10') {
			num_val = percentFormat(val/totalPop);
		}
		else if (fieldID=='oner_10'|| fieldID=='white_or10'|| fieldID=='bl_or10'|| fieldID=='aian_or10'|| 
			fieldID=='asia_or10'|| fieldID=='somoth_or1') {
			num_val = percentFormat(val/tempData.properties['totpop10']);  
		}
		else if (fieldID=='tothu10') {
			num_val = percentFormat(val/housingTotals);  
		}
		else if (fieldID=='totoccu_10'||fieldID=='totvach_10') {
			num_val = percentFormat(val/tempData.properties['tothu10']);    
		}
		else { 
			num_val = val;
		}

		if (second_fieldID=='totpop10') {
			second_num_val = percentFormat(second_val/totalPop);
		}
		else if (second_fieldID=='oner_10'|| second_fieldID=='white_or10'|| second_fieldID=='bl_or10'|| 
			second_fieldID=='aian_or10'|| second_fieldID=='asia_or10'|| second_fieldID=='somoth_or1') {
			second_num_val = percentFormat(second_val/tempData.properties['totpop10']);  
		}
		else if (second_fieldID=='tothu10') {
			second_num_val = percentFormat(second_val/housingTotals);  
		}
		else if (second_fieldID=='totoccu_10'||second_fieldID=='totvach_10') {
			second_num_val = percentFormat(second_val/tempData.properties['tothu10']);    
		}
		else { 
			second_num_val = second_val;
		}

		$("#ttCityText").text(countryNamesByCode[global_iso3]);
		$("#ttPercentText").text(num_val);
		$("#ttDescText").text(fieldText);
		$("#ttPercentText2").text(second_num_val);
		$("#ttDescText2").text(second_fieldText);
	}

	function hideTooltip() {
		// $("#tooltip").text("").css("display", "none");
	}

	function updateColorLegend(data) {
		var totalVal;
		var fieldID = textToID($(".mapTab #dd-text2").text());

		mink = d3.min(data, function(d) { 
			// Get Total Value for Getting right color code
			totalVal = getTotalVal(fieldID,d);
			return d.properties[fieldID]/totalVal;
		});

	    maxk = d3.max(data, function(d) {
			// Get Total Value for Getting right color code
	    	totalVal = getTotalVal(fieldID,d);
			return d.properties[fieldID]/totalVal;
		});

		var svg = d3.select("#colorSVG");
		var width = svg.select("rect[id='colorGradRect']").attr("width");

		svg.selectAll("text").remove();
		for (i=0; i<5; i++) {
			svg.select("rect[id="+'text'+i+"]").remove();
		}

		for (i = 0; i < 5; i++) {
			svg.append("rect").attr("id",'text'+i).attr("x", i*(width/4))
			.attr("y", -15).attr("width", 1).attr("height", 30).attr("fill", "#BBBBB7");
			svg.append("text").attr("x", (i*(width/4)-10)).attr("y", -20).attr("fill", "#A7A9AC").attr("font-size",10)
			.text(spercentFormat(mink + i*(maxk-mink)/4));
		}
	}

	function updateCircleLegend() {
		var colHeight = $("#mapCol").height();
		$("circle-legend").height(colHeight - heightOffset);
		var container = d3.select("#circle-legend");
		var margin = {left:25, top:colHeight-105};
		var maxr = rscale.range()[1];
		var w = $("#legendCol").width(), h = colHeight - heightOffset;
		var svg, defs, g = container.select("g.circle-legend"), itemEnter;
	  	var entries = [0, 0.3, 0.6, 1];
	  	var opacArr = [1, 0.8, 0.6, 0.4];
	  	var cyArr = [0,0,1,2];
	  	var posArr = [];
	  	var i = -1, j = -1, k = -1;

	  	if (g.empty()) {
	    	svg = container.append("svg")
	    	.attr("width", w)
	    	.attr("height", h)
	    	.attr("fill", "#42A5F5");

	    	g = svg.append("g")
	        .attr("class", "circle-legend")
	        .attr("transform", "translate("+margin.left+","+margin.top+")")
	  	}

	  	itemEnter = g.selectAll("g.item").data(entries).enter().append("g").attr("class", "item");
	  	itemEnter.append("rect").attr("x", maxr).attr("width", 60).attr("height", 1).attr("fill", "#A7A9AC");
		itemEnter.append("circle").attr("cx", maxr).attr("fill", "#5A5A5A");
		itemEnter.append("text").attr("x", maxr + 60 + 5).attr("fill", "#A7A9AC").attr("font-size",11);
		var items = g.selectAll("g.item").attr("transform", function(d) { return "translate(0,"+(maxr * 2 - 2*rscale(d*400000))+")"; });

	  	items.select("circle"); // propagate data update from parent
	  	items.selectAll("circle")
	  	.attr("cy",  function(d) {
	  		j = j+1;
	  		return cyArr[j]; 
	  	})
	  	.attr("r", function(d) { 
	  		k = k+1;
	  		posArr[k] = -rscale(3*d*400000);
	  		return rscale(3*d*400000); 
	  	}) 
	  	.attr("fill-opacity", function(d) {
	  		i = i+1;
	  		return opacArr[i];
	  	});
		
		k = -1;

	  	items.select("text");  // propagate data update from parent
	  	items.selectAll("text")
	  	.text(function(d) { return spercentFormat(d)})
	  	.attr("y", function(d) {
	  		k = k+1;
	  		return posArr[k]+3;
	  	})

	  	k = -1;

	  	items.select("rect");
	  	items.selectAll("rect")
	  	.attr("y", function(d) { 
	  		k = k+1;
	  		if (k == 0)
	  			return posArr[k]-2;
	  		return posArr[k]+1;
	  	});
	}

	$("#toolTipCol").height($("#mapCol").height());
	$("#circleLegendCol").height($("#mapCol").height() - heightOffset);
	var global_iso3 = null, tempData;

	queue()
	.defer(d3.json, "data/cities_wgs84.json")
	.defer(d3.csv, "data/cities_wgs84_point.csv")
  	.await(function(err, world, remittances) {
		var center = d3.geo.centroid(world);
		var scale  = 150;
		w = $("#mapCol").width();
		var w_sec = $("#mapCol_Sec").width();
		var h_sec = $("#mapCol_Sec").height()
		features = world.features;
		var offset = [w, h];
		var offset_sec = [w_sec, h_sec];
		var projection = d3.geo.mercator().scale(scale).center(center).translate(offset);
		var projection_sec = d3.geo.mercator().scale(scale).center(center).translate(offset_sec);

		var path = d3.geo.path().projection(projection);
		var path_sec = d3.geo.path().projection(projection_sec);

		var bounds = path.bounds(world);
		var hscale = scale*w/(bounds[1][0] - bounds[0][0]);
		var vscale = scale*h/(bounds[1][1] - bounds[0][1]);
		var scale  = (hscale<vscale) ? hscale : vscale;
		offset = [w-(bounds[0][0]+bounds[1][0])/2, h-(bounds[0][1]+bounds[1][1])/2];

		projection = d3.geo.mercator().center(center).scale(scale).translate(offset);
		// projection_sec = d3.geo.mercator().center(center).scale(scale).translate(offset);

		path = path.projection(projection);
		path_sec = path_sec.projection(projection_sec);
		
		var fitMapProjection = function() {
			// API Call- fitProjection(projection, data, box, center)
	    	fitProjection(projection, world, [[0, 0], [width, 0.96*height]], true);
	    	fitProjection(projection_sec, world, [[10,10], [0.95*width_sec, 0.95*height_sec]], true);
	    };

	    fitMapProjection();

	    chart_svg.append("g")
        .attr("class", "map")
        .selectAll("path")
        .data(world.features)
        .enter().append("path")
        .attr("class", "land")
		.attr({
			'opacity': 0.8
		})
        .attr("data-code", function(d) { 
        	// initialize tempData with Atlanta City Data
        	if (d.properties.NAME10 == "Atlanta")
        		tempData = d;
        	return d.properties.GEOID10; 
        })
        .on("click", function(d) { 
        	selectCountry(d.properties.GEOID10, world.features, false, true); 
        })
        .on("mouseover", function(d) { 
        	highlightCountry(d.properties.GEOID10, world.features, true); 
        })
        .on("mouseout", function(d) { 
        	highlightCountry(null, world.features, true); 
        });	
			 
		chart_sec_svg.append("g")
        .attr("class", "map")
        .selectAll("path")
        .data(world.features)
        .enter().append("path")
        .attr("class", "land")
		.attr({
			'opacity': 0.8
		})
        .attr("data-code", function(d) { return d.properties.GEOID10; })
        .on("click", function(d) { 
        	selectCountry(d.properties.GEOID10, world.features, false, false);
        	selectBar(d.properties.GEOID10); 
        	updateDetails();
        })
        .on("mouseover", function(d) { 
        	if (selectedBar == null) {
        		highlightCountry(d.properties.GEOID10, world.features, false); 
        		updateDetails();
        		updateBar(d.properties.GEOID10);
        	}
        })
        .on("mouseout", function(d) {
        	if (selectedBar == null) {
	        	highlightCountry(null, world.features, false);
	        	updateDetails();
	        	updateBar(null); 
	        }
        });

	    var updateMap = function() {
	    	chart_svg.selectAll("g.map path")
	    	.attr("d", path);

	    	chart_sec_svg.selectAll("g.map path")
	    	.attr("d", path_sec);
	    };

	    updateMap();

	    var gcountries = chart_svg
	    .append("g")
	    .attr("class", "countries");

	    maxRemittanceValue = d3.max(remittances, function(d) {
			return d.totpop10;
	    });

	    rscale.domain([0, maxRemittanceValue]);
	    initCountryNames(remittances);
	    initCountriesTypeahead(world.features);

	    circles = gcountries.selectAll("circle")
	        .data(remittances)
	        .enter()
			.append("svg:circle")
	        .attr("class", "country")
	        .attr("r", "2")
	        .attr("opacity", 0.7)
	        .attr("fill", "#5A5A5A")
	        .on("click", function(d) { selectCountry(d.GEOID10, world.features, false, true); })
	        .on("mouseover", function(d) { highlightCountry(d.GEOID10, world.features, true); })
	        .on("mouseout", function(d) { highlightCountry(null, world.features, true); });

	    var updateBubblePositions = function() {
	    	gcountries.selectAll("circle")
	      	.attr("cx", function(d) { return projection(d.centroid)[0] })
	        .attr("cy", function(d) { return projection(d.centroid)[1] });
	    };
		
		totalPop = calcTotalsByCounty(remittances,'totpop10');
	    totalPopArr = calcTotalsByCity(remittances,'totpop10');
		housingTotals = calcTotalsByCounty(remittances,'tothu10');
	    housingTotalsbycity = calcTotalsByCity(remittances,'tothu10');
		occuTotals = calcTotalsByCounty(remittances,'totoccu_10');
	    occuTotalsbycity = calcTotalsByCity(remittances,'totoccu_10');
		vacantTotals = calcTotalsByCounty(remittances,'totvach_10');
	    vacantTotalsbycity = calcTotalsByCity(remittances,'totvach_10');
		
	    updateBubblePositions();
		selectYear(2010);
		updateColorLegend(world.features);
	    updateCircleLegend();
		$("#circle-legend").fadeIn();
		
		updateColorForFirstMap(world.features);
		updateSecondMap(world.features);
    	$("#loading").hide();

    	$("#chart g.map path.land").on("mousemove", function(e){
		    var d = e.target.__data__;
		    tempData = d;
		    var iso3 = (d.properties.GEOID10 || d.GEOID10);
		    global_iso3 = iso3;
		    var num_val, second_val, second_num_val;
	    	var fieldID = textToID($(".mapTab #dd-text").text());
	    	var second_fieldID = textToID($(".mapTab #dd-text2").text());
	    	var fieldText = $(".mapTab #dd-text").text();
	    	var second_fieldText = $(".mapTab #dd-text2").text();

	        if (highlightedCountry != null) {
			 	val = (d.properties[fieldID]);
			 	second_val = (d.properties[second_fieldID]);

				if (fieldID=='totpop10') {
					num_val = percentFormat(val/totalPop);
				}
				else if (fieldID=='oner_10'|| fieldID=='white_or10'|| fieldID=='bl_or10'|| fieldID=='aian_or10'|| 
					fieldID=='asia_or10'|| fieldID=='somoth_or1') {
					num_val = percentFormat(val/d.properties['totpop10']);  
				}
				else if (fieldID=='tothu10') {
					num_val = percentFormat(val/housingTotals);  
				}
				else if (fieldID=='totoccu_10'||fieldID=='totvach_10') {
					num_val = percentFormat(val/d.properties['tothu10']);    
				}
				else { 
					num_val = val;
				}

				if (second_fieldID=='totpop10') {
					second_num_val = percentFormat(second_val/totalPop);
				}
				else if (second_fieldID=='oner_10'|| second_fieldID=='white_or10'|| second_fieldID=='bl_or10'|| 
					second_fieldID=='aian_or10'|| second_fieldID=='asia_or10'|| second_fieldID=='somoth_or1') {
					second_num_val = percentFormat(second_val/tempData.properties['totpop10']);  
				}
				else if (second_fieldID=='tothu10') {
					second_num_val = percentFormat(second_val/housingTotals);  
				}
				else if (second_fieldID=='totoccu_10'||second_fieldID=='totvach_10') {
					second_num_val = percentFormat(second_val/tempData.properties['tothu10']);    
				}
				else { 
					second_num_val = second_val;
				}

				$("#ttCityText").text(countryNamesByCode[iso3]);
				$("#ttPercentText").text(num_val);
				$("#ttDescText").text(fieldText);
				$("#ttPercentText2").text(second_num_val);
				$("#ttDescText2").text(second_fieldText);
	        }

    	}).on("mouseout", hideTooltip);

    	$("#chart g.countries circle").on("mousemove", function(e) {
	    	var d = e.target.__data__;
	    	var iso3 = d.GEOID10;
	    	global_iso3 = iso3
	    	var num_val;
			var fieldID = textToID($(".mapTab #dd-text").text());
			var fieldText = $(".mapTab #dd-text").text();

        	if (highlightedCountry != null) {
				val =  d[fieldID];
	  
		  		if (fieldID=='totpop10'){
	      			num_val = percentFormat(val/totalPop);
		  		}
		  		else if (fieldID=='oner_10'|| fieldID=='white_or10'|| fieldID=='bl_or10'|| fieldID=='aian_or10'|| fieldID=='asia_or10'|| fieldID=='somoth_or1'){
					num_val = percentFormat(val/d['totpop10']);  
		  		}
		  		else if (fieldID=='tothu10'){
					num_val = percentFormat(val/housingTotals);  
		  		}
		  		else if (fieldID=='totoccu_10'||fieldID=='totvach_10'){
					num_val = percentFormat(val/d['tothu10']);    
		  		}
		  		else{
			  		num_val = val;
		  		}
	  			$("#ttCityText").text(countryNamesByCode[iso3]);
				$("#ttNumberText").text(num_val);
				$("#ttDescText").text(fieldText);
            }

    	}).on("mouseout", hideTooltip);

		$("#details").fadeIn();
	    updateToolTip();

		var onResize = function() {
		initSizes();
		fitMapProjection();
		updateMap();
		updateBubblePositions();
		updateBubbleSizes();
		updateCircleLegend();
		};
		
		$(window).resize(onResize);
	});

	function stringParser(strArr) {
		var res = "";
		for (i = 2; i < strArr.length; i++) {
			var temp = strArr[i];
			if (i != strArr.length-1)
				temp = temp.concat(" "); 
    		res = res.concat(temp);
		}
		return res;
	}
});