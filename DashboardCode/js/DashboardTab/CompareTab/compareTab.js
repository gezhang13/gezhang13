$(document).ready(function() { 
	var topicarray = ["Percent of Fulton Population"]
	var collectarray = ["Percent of Fulton Population"]
	var color = d3.scale.category20();
	//console.log(getTextWidth('xxmanxxxxxxxxxx','19px Roboto'))
// Set up the menu 
	$(".compareTab #menu6 li a").click(function(){
		var strArray = $(this).text().split(" ");
		var parsedString = stringParser(strArray);
		//$(".compareTab #dropDownText").text(parsedString);
		$(".compareTab #dd-text").text($(this).text());
		var fieldVal = textToID($(this).text()); 
		
		topicarray=[($(this).text())]
		
   	});
	


	//functions
	function getTextWidth(text, font) {
		// if given, use cached canvas for better performance
		// else, create new canvas
		var canvas = getTextWidth.canvas || (getTextWidth.canvas = document.createElement("canvas"));
		var context = canvas.getContext("2d");
		context.font = font;
		var metrics = context.measureText(text);
		return metrics.width;
	}

	
	var spercentFormat = function(v) {
	  var spercent_fmt = d3.format(",.1%");
	  return spercent_fmt(v);
	};
	
	function stringParser(strArr) {
		var res = "";
		for (i = 2; i < strArr.length; i++) {
			var temp = strArr[i];
			if (i != strArr.length-1)
				temp = temp.concat(" "); 
    		res = res.concat(temp);
		}
		return res;
	}
	
	function truncateString(strArr) {
		var res = "";
		for (i=0; i<strArr.length-1; i++) {
			var temp = strArr[i];
			if (i < strArr.length-2)
				temp = temp.concat(" ");
			res = res.concat(temp);
		}
		return res;
	}
	
	function wrap(text, width) {
		text.each(function() {
			var text = d3.select(this),
			    words = text.text().split(/\s+/).reverse(),
			    word,
			    line = [],
			    lineNumber = 0,
			    lineHeight = 1.1, // ems
			    y = text.attr("y"),
			    dy = parseFloat(text.attr("dy")),
			    tspan = text.text(null).append("tspan").attr("x", 0).attr("y", y).attr("dy", dy + "em");
			while (word = words.pop()) {
				line.push(word);
				tspan.text(line.join(" "));
				if (tspan.node().getComputedTextLength() > width) {
					line.pop();
					tspan.text(line.join(" "));
					line = [word];
					tspan = text.append("tspan").attr("x", 0).attr("y", y).attr("dy", ++lineNumber * lineHeight + dy + "em").text(word);
				}
			}
		});
	}
	
	function getLabel(indicator) {
		var textTick = {};
		
		return textTick;
	}
	
	function textToID(strVal) {
		var res = "";
		if (strVal == "Percent of Fulton Population") {
			res = "totpop10";
		}
		else if(strVal == "Percent of One Race Total") {
			res = "oner_10";
		}
		else if(strVal == "Percent of White") {
			res = "white_or10";
		}
		else if(strVal == "Percent of Black") {
			res = "bl_or10";
		}
		else if(strVal == "Percent of American Indian") {
			res = "aian_or10";
		}
		else if(strVal == "Percent of Asian") {
			res = "asia_or10";
		}
		else if(strVal == "Percent of Some Other Race") {
			res = "somoth_or1";
		}
		else if(strVal == "Percent of Fulton Housing Units") {
			res = "tothu10";
		}
		else if(strVal == "Percent of Total Occupied Units") {
			res = "totoccu_10";
		}
		else if(strVal == "Percent of Total Vacant Units") {
			res = "totvach_10";
		}
		return res;
	}
	
	

		
// set up second row
	d3.select("#tab5")
	  .append("div")
	  .attr("id","compareSecondRow")
	  .style({"width":$('.well').width()+"px","height":$('.well').height()*0.2+"px"})
	  .style({"margin-left":'15px',"margin-top":'20px'})
	  
//tags at the second row	  
	function dynamicaddtags(){
	idcount++;
	stringlength = getTextWidth($(".compareTab #dd-text").text(),'14px Roboto')+100;
	var tagarea = d3.select(".compareTab #compareSecondRow")
				.append("svg")
				.attr("id","tag"+idcount)
				.attr("width", stringlength)
				.attr("height", 30)
					
		tagarea.append("rect")
				.attr("width", stringlength)
				.attr("height", 30)
				.attr("x", 0)
				.attr("y", 0)
				.attr("fill", "none")
				
		tagarea.append("rect")
				.attr("width", 10)
				.attr("height", 30)
				.style({"fill":currenttagcolor})
				
		tagarea.append("rect")
			.attr("width", stringlength-40)
			.attr("height", 30)
			.attr("x", 10)
			.attr("y", 0)
			.attr("fill", "#EEEEEE")
			.attr("stroke","gray")

		tagarea.selectAll("text")
			.data(topicarray)
			.enter()
			.append("text")
			.attr("id","text"+idcount)
			.attr("x", 20)
			.attr("y", 20)
			.text( function (d) { return d; })
			.attr("font-family", "Roboto")
			.attr("font-size", "14px")
			.attr("fill", "#A7A9AC");

		tagarea.append("g")
			.append("svg:image")
			.attr("xlink:href","././img/remove.png")
			.attr("width", 20)
			.attr("height", 20)
			.attr("class","removes")
			.attr("id","remove"+idcount)
			.attr("x",stringlength-65)
			.attr("y",5)
			.style({"cursor": "pointer"});
			
			
			$(".compareTab #compareSecondRow .removes").click(function (e) {
				tagcount=tagcount-1;
				d3.select("#compareFirstRow").selectAll("svg").style("cursor", "pointer")
				addbutton.selectAll("circle").style("fill", "#ED6A45")
				d3.select("#compareFirstRow").selectAll("svg").style("cursor", "pointer")
				addbutton.selectAll("circle").style("fill", "#ED6A45")

				if (collectarray.indexOf($("#tag"+(e.target.id).slice(-1)).text()) > -1){
						collectarray.splice(collectarray.indexOf($("#tag"+(e.target.id).slice(-1)).text()),1)
					}
				d3.select('#tag'+ (e.target.id).slice(-1)).remove()
				d3.select('#dot'+ (e.target.id).slice(-1)).remove()
			});

	}
	
//initialize the first state 
	var stringlength = getTextWidth($(".compareTab #dd-text").text(),'14px Roboto')+100;
	//console.log(stringlength)
	var idcount = 0;

	var colorcount = 0;
	var tagcount = 1;
	var currenttagcolor = color(colorcount);
	
	// Plus button on the left side of the menu  6 tags is max
			
	var addbutton = d3.select("#compareFirstRow")
	  .append("svg")
	  .attr("width", 30)
	  .attr("height", 30)
	  .style({"float":'right',"margin-right":'10px',"cursor": "pointer"})
			  
	var tagarea = d3.select(".compareTab #compareSecondRow")
					.append("svg")
					.attr("id","tag"+idcount)
					.attr("width", stringlength)
					.attr("height", 30)
		
		
			
		tagarea.append("rect")
				.attr("width", stringlength)
				.attr("height", 30)
				.attr("x", 0)
				.attr("y", 0)
				.attr("fill", "none")
				
		tagarea.append("rect")
				.attr("width", 10)
				.attr("height", 30)
				.style({"fill":currenttagcolor})
				
		tagarea.append("rect")
			.attr("width", stringlength-40)
			.attr("height", 30)
			.attr("x", 10)
			.attr("y", 0)
			.attr("fill", "#EEEEEE")
			.attr("stroke","gray")
			
		tagarea.selectAll("text")
				.data(topicarray)
				.enter()
				.append("text")
				.attr("id","text"+idcount)
				.attr("x", 20)
				.attr("y", 20)
				.text( function (d) { return d; })
				.attr("font-family", "Roboto")
				.attr("font-size", "14px")
				.attr("fill", "#A7A9AC");

		tagarea.append("g")
				.append("svg:image")
				.attr("xlink:href","././img/remove.png")
				.attr("class","removes")
				.attr("id","remove"+idcount)
				.attr("width", 20)
				.attr("height", 20)
				.attr("x",stringlength-65)
				.attr("y",5)
				.style({"cursor": "pointer"});

		$(".compareTab #compareSecondRow .removes").click(function (e) {

			tagcount=tagcount-1;
			d3.select("#compareFirstRow").selectAll("svg").style("cursor", "pointer")
			addbutton.selectAll("circle").style("fill", "#ED6A45")
			d3.select("#compareFirstRow").selectAll("svg").style("cursor", "pointer")
			addbutton.selectAll("circle").style("fill", "#ED6A45")

			if (collectarray.indexOf($("#tag"+(e.target.id).slice(-1)).text()) > -1){
					collectarray.splice(collectarray.indexOf($("#tag"+(e.target.id).slice(-1)).text()),1)
				}
			d3.select('#tag'+ (e.target.id).slice(-1)).remove()
			d3.select('#dot'+ (e.target.id).slice(-1)).remove()
		});			
		
// scatterplot chart
    var ThirdRowheight = $('.well').height()*0.7;
	var ThirdRowwidth = $('.well').width();

	d3.select("#tab5")
	  .append("div")
	  .attr("id","compareThirdRow")
	  .style({'width':ThirdRowwidth+'px','height':ThirdRowheight+'px', 'margin-left':'15px',"margin-top":'20px'});
	  
	var margin = {top: -40, right: 10, bottom: 10, left: 50},
		width = $('.compareTab #compareThirdRow').width(),
		height = $('.compareTab #compareThirdRow').height()-30;
	
	var plotsvg = d3.select(".compareTab #compareThirdRow").append("svg")
		.attr("width", width*0.95)
		.attr("height", height+30)
		.append("g")
		.attr("transform", "translate(" + margin.left + "," + margin.top + ")");
		
	var xScale = d3.scale.ordinal().rangeRoundBands([0, width*0.95 - margin.left -20], .2),
		yScale = d3.scale.linear().range([height, 50]),
		xAxis = d3.svg.axis().scale(xScale).orient("bottom").tickSize(0,0),  
		yAxis = d3.svg.axis().scale(yScale).orient("left").tickFormat(spercentFormat).tickSize(-width*0.95,0).ticks(4);
				
	var dot = plotsvg.append("g")
		.attr("class", "dots")
		.attr("id","dot"+idcount);
			  
	function calcTotalsByCounty(values, index) {
		var totals, i, yi, countryData, y, val, max = NaN;

		for (i=0; i<values.length; i++) {
			countryData = values[i];
			val = str2num(countryData[index]);
			if (!isNaN(val)) {
				if (totals === undefined) totals = 0;
				totals += val;
			}
		}
		return totals;
	}
	
	function str2num(str) {
	  // empty string gives 0 when using + to convert
	  if (str === null || str === undefined || str.length == 0) return NaN;
	  return +str;
	}
	
	function pickIndicatorData(dataset, indicator) {
			var populationTotals = calcTotalsByCounty(dataset,'totpop10');
			var housingTotals = calcTotalsByCounty(dataset,'tothu10');
				//console.log(indicator)
			$.each(dataset, function(i,d) {	
		
				d.value = d[indicator];	
				d.totpop10 = d['totpop10'];
				d.housing = d['tothu10']
				d.populationTotals = populationTotals;
				d.housingTotals = housingTotals;
			});
			return dataset;
	}

	function position(dot) {

		dot.attr("cx", function(d) { return xScale(d.name); })
		   .attr("cy", function(d) { return yScale(parseInt(d.totpop10)/parseInt(d.populationTotals)); })
		   .attr("r", 5);
	
	}
	
	function CreateScatterPlotChart(data,textTick){
			var miny = d3.min(data, function(d) { return parseInt(d.totpop10)/parseInt(d.populationTotals); });
			var maxy = d3.max(data, function(d) { return parseInt(d.totpop10)/parseInt(d.populationTotals); });
			xScale.domain(data.map(function(d) { 
	    	var arr = d.name.split(" ");
	    	var shortStr = truncateString(arr);
	    	return shortStr; 
			}));
			
			yScale.domain([0, 1]);
			plotsvg.append("g")
				.attr("class", "x axis")
				.attr("transform", "translate(0," + height + ")")
				.call(xAxis)
				.call(function(selections) {
					selections.selectAll('.tick text')
					.attr('transform', 'translate(-15,33) rotate(315)')
					.attr('fill','#AEAEAE')
					.attr('font-size','9px')
					.attr('font-family','Roboto');
					selections.selectAll('.tick line')
					.attr('y2','0');
				})
				.attr("transform", "translate(0," + height +30 + ")")
				.selectAll(".tick text")
				.call(wrap, xScale.rangeBand());
						
			plotsvg.append("g")
				.attr("class", "y axis")
				.call(yAxis)
				.call(function(selections) {
					selections.selectAll('.tick text')
					.attr('x',-5)
					.attr('fill','#8F8F8F')
					.attr('font-size','11px')
					.attr('font-family','Roboto');
				})
				.append("text")
				.attr("class", "axisText")
				.attr("transform", "rotate(-90)")
				.attr("y", 1)
				.attr("x", 40)
				.attr("dy", ".71em")
				.style("text-anchor", "end")
				.style("fill", "red")
				.text(textTick.text);
		
		
			dot.selectAll(".dot")
				.data(data)
				.enter()
				.append("circle")
				.attr("class", "dot")
				.style("fill", currenttagcolor)
				.style("stroke","white")
				.call(position)
				.on('mouseover', function (d){
				 tooltip.html(
				 '<h5>' + d.name + '</h5>' +
				 '<br>'+
				'<h5>' + spercentFormat(parseInt(d.totpop10)/parseInt(d.populationTotals)) + '</h5>'+ 
				'<h5>' + $(".compareTab #dd-text").text() + '</h5>'
				)	
                .style("left", (d3.event.pageX-450) + "px")		
                .style("top", (d3.event.pageY-180 ) + "px")
				.style("opacity", 0.8);	
				  })
				.on('mouseout', function(){
						tooltip.transition()		
						.duration(500)		
						.style("opacity", 0);		
				  })
	}
	
	function updateScatterPlotChart(data,textTick){
	
		dot = plotsvg.append("g")
			.attr("class", "dots")
			.attr("id","dot"+idcount);
			
		var fieldID = textToID($(".compareTab #dd-text").text());
		if (fieldID=='totpop10') {
			dot.selectAll(".dot")
			   .data(data)
			   .enter()
			   .append("circle")
			   .attr("class", "dot")
			   .style("fill", currenttagcolor)
			   .style("stroke","white")
			   .attr("cx", function(d) { return xScale(d.name); })
			   .attr("cy", function(d) { return yScale(parseInt(d.totpop10)/parseInt(d.populationTotals)); })
			   .attr("r", 5)
			   .on('mouseover', function (d){
				 tooltip.html(
				 '<h5>' + d.name + '</h5>' +
				 '<br>'+
				'<h5>' + spercentFormat(parseInt(d.totpop10)/parseInt(d.populationTotals)) + '</h5>'+ 
				'<h5>' + $(".compareTab #dd-text").text() + '</h5>'
				)	
                .style("left", (d3.event.pageX-450) + "px")		
                .style("top", (d3.event.pageY-180 ) + "px")
				.style("opacity", 0.8);	
				  })
				.on('mouseout', function(){
						tooltip.transition()		
						.duration(500)		
						.style("opacity", 0);		
				  });
		}
		else if (fieldID=='oner_10'|| fieldID=='white_or10'||
			fieldID=='bl_or10'||fieldID=='aian_or10'||
			fieldID=='asia_or10'||fieldID=='somoth_or1') {
			dot.selectAll(".dot")
				  .data(data)
				  .enter()
				  .append("circle")
				  .attr("class", "dot")
				  .style("fill", currenttagcolor)
				  .style("stroke","white")
				  .attr("cx", function(d) { return xScale(d.name); })
				  .attr("cy", function(d) { return yScale(parseInt(d.value)/parseInt(d.totpop10)); })
				  .attr("r", 5).on('mouseover', function (d){
				 	
                tooltip.html( 
				'<h5>' + spercentFormat(parseInt(d.value)/parseInt(d.totpop10)) + '</h5>'+ 
				'<h5>' + $(".compareTab #dd-text").text() + '</h5>'
				)
				.style("left", (d3.event.pageX-450) + "px")		
                .style("top", (d3.event.pageY-180 ) + "px")
				.style("opacity", 0.8);	
				  })
				.on('mouseout', function(){
						tooltip.transition()		
						.duration(500)		
						.style("opacity", 0);		
				  });
		}
		else if (fieldID=='tothu10') {		
			dot.selectAll(".dot")
			  .data(data)
			  .enter()
			  .append("circle")
			  .attr("class", "dot")
			  .style("fill", currenttagcolor)
			  .style("stroke","white")
				.attr("cx", function(d) { return xScale(d.name); })
			   .attr("cy", function(d) { return yScale(parseInt(d.value)/parseInt(d.housingTotals)); })
			   .attr("r", 5);
		}
		else if (fieldID=='totoccu_10'||fieldID=='totvach_10') {
			dot.selectAll(".dot")
			  .data(data)
			  .enter()
			  .append("circle")
			  .attr("class", "dot")
			  .style("fill", currenttagcolor)
			  .style("stroke","white")
				.attr("cx", function(d) { return xScale(d.name); })
			   .attr("cy", function(d) { return yScale(parseInt(d.value)/parseInt(d.housing)); })
			   .attr("r", 5);			
		}
		else {
			dot.selectAll(".dot")
			  .data(data)
			  .enter()
			  .append("circle")
			  .attr("class", "dot")
			  .style("fill", currenttagcolor)
			  .style("stroke","white")
				.attr("cx", function(d) { return xScale(d.name); })
			   .attr("cy", function(d) { return yScale(parseInt(d.value)); })
			   .attr("r", 5);
		}		  
	}
	
	var tooltip =  d3.select(".compareTab").append('div')
	.attr("class", "pinwheelTooltip")
			   .style('opacity', 0);
	
	d3.json("data/county_plotchart.json", function(data) { 	
			var origindata = data;
			var datasetIndicators = origindata;
			// pick initial indicator
			var indicator = textToID($(".compareTab #dd-text").text());
			// pick initial order, set indicator to d.value and order for plotting
			var filteredDataByIndicator = pickIndicatorData(datasetIndicators,indicator);
			// get text and ticks
			var textTick = getLabel(indicator);
			// create bubble chart
			CreateScatterPlotChart(filteredDataByIndicator,textTick)
			
			
			  
			  addbutton.append("circle")
			  .attr("cx", 15)
			  .attr("cy", 15)
			  .attr("r", 15)
			  .style("fill", "#ED6A45");
			  
			  addbutton.append("g")
			  .append("svg:image")
			  .attr("xlink:href","././img/add.png")
			  .attr("width", 20)
			  .attr("height", 20)
			  .attr("x",5)
			  .attr("y",5)
			  .on("click", function(){
					colorcount++;
					currenttagcolor = color(colorcount);
					
					if (tagcount >= 6){
						d3.select("#compareFirstRow").selectAll("svg").style("cursor", "not-allowed")
						addbutton.selectAll("circle").style("fill", "gray")
					}else{
						if (collectarray.indexOf($(".compareTab #dd-text").text()) > -1){
							alert("Already Seleted")
						}else{
							tagcount++;
							collectarray.push($(".compareTab #dd-text").text())
							
				
							dynamicaddtags();
							var indicator = textToID($(".compareTab #dd-text").text());
							var filteredDataByIndicator = pickIndicatorData(datasetIndicators,indicator);
							var textTick = getLabel(indicator);
							updateScatterPlotChart(filteredDataByIndicator,textTick)
						}
					}
					
				});
				
		});
		
	 
});
