$(document).ready(function() {
	$("#censusMenu li a").click(function(){
		$(".censusTab #dd-text").text($(this).text());
		// if (features != null) {
		// 	updateColorForFirstMap(features);
		// 	updateColorLegend(features);
		// 	updateToolTip();
		// }
   	});

   	var censusChartSVG = d3.select("#censusChart").append("svg");
	var background = censusChartSVG.append("rect");
	var mapColors = d3.scale.linear()
		.range(["#E0E0E0","#212121"])
		.interpolate(d3.interpolateHcl);

	function initSizes() {
		width_sec = $("#mapCol_Sec").width();
		height_sec = $("#mapCol_Sec").height(); 
		background.attr("width", width_sec).attr("height", height_sec);
		censusChartSVG.attr("width", width_sec).attr("height", height_sec);
	};

	initSizes();

	var isPlural = function(v, exp) {
		var v = Math.abs(Math.round(v/exp));
		return v >= 2;
	}

	var numberFormat = (function() {
		var short_fmt = d3.format(",.0f");
		var nfmt = d3.format(",.1f");
		var fmt = function(v) {  // remove trailing .0
			var formatted = nfmt(v);
			var m = formatted.match(/^(.*)\.0$/);
			if (m !== null) formatted = m[1];
				return formatted;
		};
		return function(v) {
		if (v == null  ||  isNaN(v)) return msg("amount.not-available");
		if (isPlural(v, 1e9)) return msg("amount.billions",  fmt(v / 1e9));
		if (v >= 1e9) return msg("amount.billions.singular",  fmt(v / 1e9));
		if (isPlural(v, 1e6)) return msg("amount.millions",  fmt(v / 1e6));
		if (v >= 1e6) return msg("amount.millions.singular",  fmt(v / 1e6));
			return short_fmt(v);
		};
	})();

	var moneyFormat = function(v) {
		if (v == null  ||  isNaN(v)) 
			return msg("amount.not-available");
	  	return msg("money", numberFormat(v));
	};

	var moneyMillionsFormat = function(v) { 
		return moneyFormat(1e6 * v);
	};

	function str2num(str) {
		// empty string gives 0 when using + to convert
		if (str === null || str === undefined || str.length == 0) return NaN;
		return +str;
	}

	var percentFormat = function(v) {
		var percent_fmt = d3.format(",.1%");
		return percent_fmt(v);
	};

	var spercentFormat = function(v) {
		var spercent_fmt = d3.format(",.1%");
		return spercent_fmt(v);
	};

	/* @param values is an array in which the indices correspond to the indices in the remittenceYears array */
	function calcTotalsByCounty(values, index) {
		var totals, i, yi, countryData, y, val, max = NaN;
		for (i=0; i<values.length; i++) {
			countryData = values[i];
		 	val = str2num(countryData[index]);
		  	if (!isNaN(val)) {
		    	if (totals === undefined) totals = 0;
		    	totals += val;
		  	}
		}
		return totals;
	}

	function calcTotalsByCity(values, index) {
		var totals={}, i, yi, countryData, y, val, max = NaN;

		for (i=0; i<values.length; i++) {
			countryData = values[i];
			val = str2num(countryData[index]);
		  	if (!isNaN(val)) {
		    	if (totals === undefined) totals = 0;
		    	totals[countryData.GEOID10] = val;
		  	}
		}
		return totals;
	}
	
	function updateDetails() {
		var countryName, totalPopulation, numMigrants;

		if (secondHighlightedCountry != null  ||  secondSelectedCountry != null) {
			var iso3 = (secondSelectedCountry || secondHighlightedCountry);
			countryName = countryNamesByCode[iso3];

			var countryRem = totalPopArr[iso3];
			totalPopulation = (countryRem != null ? str2num(countryRem) : NaN);
			numMigrants = housingTotalsbycity[iso3];
			totalAid = occuTotalsbycity[iso3];
			totalvacant = vacantTotalsbycity[iso3];
		} 
		else {
			totalPopulation = totalPop;
			numMigrants = housingTotals;
			totalvacant = vacantTotals;
			totalAid = occuTotals;
		}

		
		$(".censusTab #text_1_1_2_1").text(numberFormat(totalPopulation)); // population
		$(".censusTab #text_1_2_2_1").text(numberFormat(totalAid)); // occupied
		$(".censusTab #text_2_1_2_1").text(numberFormat(numMigrants)); // housing units
		$(".censusTab #text_2_2_2_1").text(numberFormat(totalvacant)); // vacant housing units
	}

	queue()
	.defer(d3.json, "data/cities_wgs84.json")
	.defer(d3.csv, "data/cities_wgs84_point.csv")
  	.await(function(err, world, remittances) {
		var center = d3.geo.centroid(world);
		var scale  = 150;
		var w_sec = $("#mapCol_Sec").width();
		var h_sec = $("#mapCol_Sec").height()
		features = world.features;
		var offset_sec = [w_sec, h_sec];
		var projection_sec = d3.geo.mercator().scale(scale).center(center).translate(offset_sec);

		var path_sec = d3.geo.path().projection(projection_sec);

		path_sec = path_sec.projection(projection_sec);
		
		var fitMapProjection = function() {
			// API Call- fitProjection(projection, data, box, center)
	    	fitProjection(projection_sec, world, [[10,10], [0.95*width_sec, 0.95*height_sec]], true);
	    };

	    fitMapProjection();
			 
		censusChartSVG.append("g")
        .attr("class", "map")
        .selectAll("path")
        .data(world.features)
        .enter().append("path")
        .attr("class", "land")
		.attr({
			'opacity': 0.8
		})
        .attr("data-code", function(d) { return d.properties.GEOID10; });

	    var updateMap = function() {
	    	censusChartSVG.selectAll("g.map path")
	    	.attr("d", path_sec);
	    };

	    updateMap();

	    var gcountries = censusChartSVG
	    .append("g")
	    .attr("class", "countries");

	    maxRemittanceValue = d3.max(remittances, function(d) {
			return d.totpop10;
	    });    	

		var onResize = function() {
		initSizes();
		fitMapProjection();
		updateMap();
		};
		
		$(window).resize(onResize);
	});
});